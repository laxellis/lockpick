import qbs

CppApplication {
        name: "lockpick"

        Group {
                fileTagsFilter: "application"
                qbs.install: true
                qbs.installDir: "bin"
        }

        Depends {
                name: "Qt"
                submodules: [
                        "core",
                        "gui",
                        "qml",
                        "quick",
                        "svg",
                ]
        }

        cpp.includePaths: [
                "sources",
                "resources/qml"
        ]
        cpp.cxxFlags: "-std=c++11"

        Properties {
                condition: qbs.hostOS.contains("windows")
                consoleApplication: false
        }

        Properties {
                condition: qbs.hostOS.contains( "osx" )
                cpp.infoPlist: ({
                        "CFBundleDevelopmentRegion": "en",
                        "CFBundleExecutable": "lockpick",
                        "CFBundleIconFile": "lockpick",
                        "CFBundleInfoDictionaryVersion": "6.0",
                        "CFBundleIdentifier": "cards.mutable.lockpick",
                        "CFBundleName": "Lockpick",
                        "CFBundlePackageType": "APPL",
                        "CFBundleShortVersionString": "0.3.0",
                        "CFBundleSignature": "????"
                })
                cpp.processInfoPlist: false
        }

        Group {
                name: "sources"
                prefix: "sources/"
                files: [
                        "main.cpp",
                        "lockpick/card.cpp",
                        "lockpick/card.hpp",
                        "lockpick/card_list.cpp",
                        "lockpick/card_list.hpp",
                        "lockpick/database.cpp",
                        "lockpick/database.hpp",
                        "lockpick/deck.cpp",
                        "lockpick/deck.hpp",
                        "lockpick/filter.cpp",
                        "lockpick/filter.hpp",
                        "lockpick/lockpick_controller.cpp",
                        "lockpick/lockpick_controller.hpp",
                        "lockpick/sorter.cpp",
                        "lockpick/sorter.hpp",
                        "lockpick/state.cpp",
                        "lockpick/state.hpp",
                        "lockpick/state_machine.cpp",
                        "lockpick/state_machine.hpp",
                        "lockpick/toggle_state.cpp",
                        "lockpick/toggle_state.hpp",
                        "lockpick/utility.cpp",
                        "lockpick/utility.hpp",
                        "lockpick/ui/color.cpp",
                        "lockpick/ui/color.hpp",
                        "lockpick/ui/font.cpp",
                        "lockpick/ui/font.hpp",
                        "lockpick/ui/image.cpp",
                        "lockpick/ui/image.hpp",
                        "lockpick/ui/ui.cpp",
                        "lockpick/ui/ui.hpp"
                ]
        }

        Group {
                name: "resources"
                prefix: "resources/"
                files: [
                        "images.qrc",
                        "resources.qrc",
                        "platforms/windows/lockpick.rc",
                        "qml/AboutDialog.qml",
                        "qml/ActionIcon.qml",
                        "qml/AppBar.qml",
                        "qml/AppBarThemeMenu.qml",
                        "qml/BackgroundDialogOverlay.qml",
                        "qml/CardQuantity.qml",
                        "qml/CardStats.qml",
                        "qml/Database.qml",
                        "qml/DatabaseItem.qml",
                        "qml/DatabaseSortMenu.qml",
                        "qml/DatabaseSortRoleMenu.qml",
                        "qml/Deck.qml",
                        "qml/DeckBottomSheet.qml",
                        "qml/DeckItem.qml",
                        "qml/DeckMoreMenu.qml",
                        "qml/DeckSortMenu.qml",
                        "qml/Divider.qml",
                        "qml/ExpandableList.qml",
                        "qml/FactionIcon.qml",
                        "qml/FactionsList.qml",
                        "qml/Filters.qml",
                        "qml/FlatButton.qml",
                        "qml/FloatingActionButton.qml",
                        "qml/Icon.qml",
                        "qml/IconAndText.qml",
                        "qml/InfluenceValue.qml",
                        "qml/Ink.qml",
                        "qml/ListTile.qml",
                        "qml/LockpickContent.qml",
                        "qml/LockpickWindow.qml",
                        "qml/Menu.qml",
                        "qml/MenuTmp.qml",
                        "qml/MenuIcon.qml",
                        "qml/MenuItem.qml",
                        "qml/Paper.qml",
                        "qml/SearchBar.qml",
                        "qml/SearchField.qml",
                        "qml/SetsList.qml",
                        "qml/SidesList.qml",
                        "qml/TextField.qml",
                        "qml/Tile.qml",
                        "qml/TitleTextField.qml",
                        "qml/ToggleListTile.qml",
                        "qml/ToolBar.qml",
                        "qml/TypesList.qml"
                ]
        }
}
