// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

Item {
        id: root

        implicitWidth: 640 * ui.dp
        implicitHeight: 480 * ui.dp

        visible: false

        Lp.BackgroundDialogOverlay {
                id: background

                anchors {
                        fill: root
                }

                //onClicked: stateMachine.hideAboutDialog()
        }

        Lp.Paper {
                id: dialog

                width: 384 * ui.dp
                height: 504 * ui.dp

                anchors {
                        centerIn: root
                }

                shadow: true

                Text {
                        id: title

                        anchors {
                                top: dialog.top
                                left: dialog.left

                                topMargin: 24 * ui.dp
                                leftMargin: 24 * ui.dp
                        }

                        text: qsTr( "About" )
                        font: ui.font.title
                        color: ui.color.textFrom( dialog.color )
                }

                Image {
                        id: lockpickIcon

                        width: 64 * ui.dp
                        height: 64 * ui.dp

                        anchors {
                                top: title.bottom
                                left: dialog.left

                                topMargin: 24 * ui.dp
                                leftMargin: 24 * ui.dp
                        }

                        source: ui.image.lockpick
                        sourceSize {
                                width: lockpickIcon.width
                                height: lockpickIcon.height
                        }
                }

                Item {
                        id: infoContainer

                        height: name.height + version.height + copyright.height
                                + 4 * ui.dp * 2

                        anchors {
                                verticalCenter: lockpickIcon.verticalCenter
                                left: lockpickIcon.right

                                leftMargin: 8 * ui.dp
                        }

                        Text {
                                id: name

                                anchors {
                                        top: infoContainer.top
                                        left: infoContainer.left
                                }

                                text: Qt.application.name
                                font: ui.font.body2
                                color: ui.color.textFrom( dialog.color )
                        }

                        Text {
                                id: version

                                anchors {
                                        top: name.bottom
                                        left: infoContainer.left

                                        topMargin: 4 * ui.dp
                                }

                                text: Qt.application.version
                                font: ui.font.body1
                                color: ui.color.secondaryTextFrom(
                                        dialog.color )
                        }

                        Text {
                                id: copyright

                                anchors {
                                        top: version.bottom
                                        left: infoContainer.left
                                        topMargin: 4 * ui.dp
                                }

                                text: "Copyright © 2015 Alexis Lecharpentier"
                                font: ui.font.body1
                                color: ui.color.secondaryTextFrom(
                                        dialog.color )
                        }
                }

                Text {
                        id: licences

                        anchors {
                                top: lockpickIcon.bottom
                                left: dialog.left
                                right: dialog.right

                                topMargin: 24 * ui.dp
                                leftMargin: 24 * ui.dp
                                rightMargin: 24 * ui.dp
                        }

                        text: "<p>Lockpick is built on the foundations of:" +
                              "</p>" +
                              "</br>" +
                              "<p><b>Qt</b> by the <i>Qt Company</i></p>" +
                              "<p><a href=\"https://www.gnu.org/licenses/" +
                              "lgpl-3.0.html\">LGPLv3</a></p>" +
                              "</br>" +
                              "<p><b>Roboto font</b> by <i>Christian " +
                              "Robertson</i></p>" +
                              "<p><a href=\"https://www.apache.org/licenses/" +
                              "LICENSE-2.0.html\">Apache License, version 2.0" +
                              "</a></p>" +
                              "</br>" +
                              "<p><b>Material Design icons</b> by " +
                              "<i>Google</i></p>" +
                              "<p><a href=\"https://creativecommons.org/" +
                              "licenses/by/4.0/\">Attribution 4.0 " +
                              "International</a></p>" +
                              "</br>" +
                              "<p>The copyrightable portions of <i>Android: " +
                              "Netrunner: The Card Game</i> and its " +
                              "expansions are © 2012-2014 <i>Wizards</i> and " +
                              "<i>Fantasy Flight Publishing, Inc</i>.</p>" +
                              "<p>All <i>Android: Netrunner: The Card Game" +
                              "</i> expansion titles, are trademarks and/or " +
                              "registered trademarks of <i>Fantasy Flight " +
                              "Publishing, Inc</i>.</p>" +
                              "<p>All rights reserved to their respective " +
                              "owners.</p>"
                        font: ui.font.body1
                        color: ui.color.textFrom( dialog.color )

                        wrapMode: Text.Wrap

                        onLinkActivated: Qt.openUrlExternally( link )
                }

                Row {
                        id: buttons

                        height: 48 * ui.dp

                        anchors {
                                bottom: dialog.bottom
                                right: dialog.right

                                bottomMargin: 8 * ui.dp
                                rightMargin: 16 * ui.dp
                        }

                        spacing: 8 * ui.dp

                        Lp.FlatButton {
                                id: cancelButton

                                anchors {
                                        verticalCenter: buttons.verticalCenter
                                }

                                text: qsTr( "Cancel" )

                                onClicked: stateMachine.hideAboutDialog()
                        }

                        Lp.FlatButton {
                                id: okButton

                                anchors {
                                        verticalCenter: buttons.verticalCenter
                                }

                                text: qsTr( "Ok" )

                                onClicked: stateMachine.hideAboutDialog()
                        }
                }

                //MouseArea {
                //        id: mouseArea

                //        anchors {
                //                fill: dialog
                //        }

                //        onClicked: {}
                //}
        }

        states: State {
                name: "Visible"
                when: stateMachine.visibleAboutDialog.isActive
                PropertyChanges {
                        target: root
                        visible: true
                }
        }
}
