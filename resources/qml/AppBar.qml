// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

Lp.Paper {
        id: root

        implicitWidth: height * 10
        height: 64 * ui.dp

        color: ui.color.appBar
        shadow: true

        Text {
                id: name

                anchors {
                        top: root.top
                        left: root.left

                        topMargin: 20 * ui.dp
                        leftMargin: 24 * ui.dp + 8 * ui.dp
                }

                text: Qt.application.name
                font: ui.font.title
                color: ui.color.textFrom( root.color )
        }

        //Lp.ActionIcon {
        //        id: themeIcon

        //        anchors {
        //                verticalCenter: root.verticalCenter
        //                right: helpIcon.left

        //                rightMargin: 24 * ui.dp
        //        }

        //        image: ui.image.style
        //        color: ui.color.iconFrom( root.color )

        //        onClicked: themeMenu.show()
        //}

        //Lp.AppBarThemeMenu {
        //        id: themeMenu

        //        anchors {
        //                top: root.top
        //                right: themeIcon.right

        //                topMargin: 8 * ui.dp
        //                rightMargin: -12 * ui.dp
        //        }
        //}

        Lp.ActionIcon {
                id: helpIcon

                anchors {
                        verticalCenter: root.verticalCenter
                        right: root.right

                        rightMargin: 20 * ui.dp + 8 * ui.dp
                }

                image: ui.image.help
                color: ui.color.iconFrom( root.color )

                onClicked: stateMachine.showAboutDialog()
        }

        //Lp.ToolBar {
        //        id: databaseToolBar

        //        anchors {
        //                bottom: root.bottom
        //                left: root.left
        //                right: root.right

        //                rightMargin: deck.width + deck.anchors.rightMargin + 24 * ui.dp
        //        }

        //        color: root.color

        //        Text {
        //                id: sortBy

        //                anchors {
        //                        baseline: databaseToolBar.bottom
        //                        left: databaseToolBar.left

        //                        baselineOffset: -24 * ui.dp
        //                        leftMargin: 24 * ui.dp
        //                }

        //                text: "Sort by"
        //                font: ui.font.title
        //                color: ui.color.secondaryTextFrom( root.color )
        //        }

        //        Text {
        //                id: sortRole

        //                anchors {
        //                        baseline: sortBy.baseline
        //                        left: sortBy.right

        //                        leftMargin: 8 * ui.dp
        //                }

        //                text: sortMenu.choice
        //                font: ui.font.title
        //                color: ui.color.textFrom( root.color )
        //        }

        //        Lp.Icon {
        //                id: dropDownIcon

        //                anchors {
        //                        top: databaseToolBar.top
        //                        bottom: databaseToolBar.bottom
        //                        left: sortRole.right

        //                        topMargin: 20 * ui.dp
        //                        bottomMargin: 20 * ui.dp
        //                }

        //                image: ui.image.dropDown
        //                color: ui.color.iconFrom( root.color )
        //        }

        //        MouseArea {
        //                id: sortMouseArea

        //                anchors {
        //                        top: databaseToolBar.top
        //                        bottom: databaseToolBar.bottom
        //                        left: sortRole.left
        //                        right: dropDownIcon.right

        //                        leftMargin: -12 * ui.dp
        //                        rightMargin: -12 * ui.dp
        //                }

        //                onClicked: sortMenu.forceActiveFocus(
        //                        Qt.MouseFocusReason )
        //        }

        //        Lp.MenuTmp {
        //                id: sortMenu

        //                anchors {
        //                        top: databaseToolBar.top
        //                        left: sortRole.left

        //                        leftMargin: -24 * ui.dp
        //                }

        //                choice: sorter.role
        //                choices: [ "Faction", "Set", "Title", "Type" ]

        //                onChoiceChanged: {
        //                        sorter.sortBy( choice )
        //                }
        //        }

        //        Lp.SearchField {
        //                id: searchField

        //                color: root.color

        //                anchors {
        //                        top: databaseToolBar.top
        //                        left: databaseToolBar.left

        //                        leftMargin: 64 * ui.dp * 5
        //                }

        //                onTextChanged: filter.search( text )
        //                onEditingFinished: database.focus = true;
        //        }

        //        Text {
        //                id: searchRole

        //                anchors {
        //                        baseline: sortBy.baseline
        //                        left: databaseToolBar.right

        //                        leftMargin: -132 * ui.dp
        //                }

        //                //text: searchMenu.choice
        //                font: ui.font.title
        //                color: ui.color.textFrom( root.color )
        //        }

        //        Lp.Icon {
        //                id: searchDropDownIcon

        //                anchors {
        //                        top: databaseToolBar.top
        //                        bottom: databaseToolBar.bottom
        //                        left: searchRole.right

        //                        topMargin: 20 * ui.dp
        //                        bottomMargin: 20 * ui.dp
        //                }

        //                image: ui.image.dropDown
        //                color: ui.color.iconFrom( root.color )
        //        }

        //        MouseArea {
        //                id: searchMouseArea

        //                anchors {
        //                        top: databaseToolBar.top
        //                        bottom: databaseToolBar.bottom
        //                        left: searchRole.left
        //                        right: searchDropDownIcon.right

        //                        leftMargin: -12 * ui.dp
        //                        rightMargin: -12 * ui.dp
        //                }

        //                onClicked: searchMenu.forceActiveFocus(
        //                        Qt.MouseFocusReason )
        //        }

        //        //Lp.Menu {
        //        //        id: searchMenu

        //        //        anchors {
        //        //                top: databaseToolBar.top
        //        //                left: searchRole.left

        //        //                leftMargin: -24 * ui.dp
        //        //        }

        //        //        choice: filter.searchRole
        //        //        choices: [ "Subtype", "Text", "Title" ]

        //        //        onChoiceChanged: {
        //        //                filter.searchBy( choice )
        //        //        }
        //        //}
        //}
}
