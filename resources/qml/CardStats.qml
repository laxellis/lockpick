// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

Row {
        id: root

        spacing: 32 * ui.dp

        Lp.IconAndText {
                id: minimumDeckSize

                visible: ( model.type === "Corporation Identity" )
                || ( model.type === "Runner Identity" )

                icon: ui.image.minimumDeckSize
                text: model.minimumDeckSize
        }

        Lp.IconAndText {
                id: influenceLimit

                visible: ( model.type === "Corporation Identity" )
                || ( model.type === "Runner Identity" )

                icon: ui.image.influenceLimit
                text: model.influenceLimit
        }

        Lp.IconAndText {
                id: baseLink

                visible: model.type === "Runner Identity"

                icon: ui.image.baseLink
                text: model.baseLink
        }

        Lp.IconAndText {
                id: rezCost

                visible: ( model.type === "Ice" )
                || ( model.type === "Asset" )
                || ( model.type === "Upgrade" )

                icon: ui.image.rez
                text: model.rezCost
        }

        Lp.IconAndText {
                id: trashCost

                visible: ( model.type === "Asset" )
                || ( model.type === "Upgrade" )

                icon: ui.image.trash
                text: model.trashCost
        }

        Lp.IconAndText {
                id: playCost

                visible: ( model.type === "Operation" )
                || ( model.type === "Event" )

                icon: ui.image.credit
                text: model.playCost
        }

        Lp.IconAndText {
                id: installCost

                visible: ( model.type === "Program" )
                || ( model.type === "Hardware" )
                || ( model.type === "Resource" )

                icon: ui.image.credit
                text: model.installCost
        }

        Lp.IconAndText {
                id: strength

                visible: ( model.type === "Ice" )
                || ( model.type === "Program" )

                icon: ui.image.strength
                text: model.strength
        }

        Lp.IconAndText {
                id: memoryCost

                visible: model.type === "Program"

                icon: ui.image.memory
                text: model.memoryCost
        }

        Lp.IconAndText {
                id: advancementRequirement

                visible: model.type === "Agenda"

                icon: ui.image.advancementRequirement
                text: model.advancementRequirement
        }

        Lp.IconAndText {
                id: agendaPoints

                visible: model.type === "Agenda"

                icon: ui.image.agendaPoints
                text: model.agendaPoints
        }
}
