// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

Lp.Tile {
        id: root

        property string title: model.title
        property string subtitle: model.subtitle
        property string type: model.type

        implicitWidth: 72 * ui.dp * 6
        height: focus ?
                72 * ui.dp
                + Math.max( primaryContent.height, secondaryContent.height )
                + 20 * ui.dp
                : 72 * ui.dp

        clip: true

        color: focus ? ui.color.cardHighlight : ui.color.card

        Image {
                id: illustration

                width: 40 * ui.dp
                height: width

                anchors {
                        top: root.top
                        horizontalCenter: root.left

                        topMargin: 16 * ui.dp
                        horizontalCenterOffset: 52 * ui.dp
                }

                asynchronous: true
                source: "/images/cards/" + model.set +
                        "/" + model.number + ".png"
                sourceSize {
                        width: width
                        height: height
                }
        }

        Text {
                id: title

                anchors {
                  top: root.top
                  left: root.left

                  topMargin: 20 * ui.dp
                  leftMargin: 104 * ui.dp
                }

                text: model.isUnique ? "• " + model.title : model.title
                font: ui.font.subhead
                color: ui.color.textFrom( root.color )
        }

        Text {
                id: subtitle

                anchors {
                        baseline: title.baseline
                        left: title.right

                        leftMargin: 4 * ui.dp
                }

                visible: ( model.type === "Corporation Identity" ) ||
                         ( model.type === "Runner Identity" )

                text: model.subtitle
                font: ui.font.subhead
                color: ui.color.secondaryTextFrom( root.color )
        }

        Text {
                id: typeAndSubtype

                function simpleType( string ) {
                        if ( ( string === "Corporation Identity" ) ||
                             ( string === "Runner Identity" ) ) {
                                     return "Identity";
                        } else {
                                return string;
                        }
                }

                anchors {
                  baseline: title.baseline
                  left: root.left

                  baselineOffset: 16 * ui.dp
                  leftMargin: 104 * ui.dp
                }

                text: model.subtype ?
                      simpleType( model.type ) + ": " + model.subtype :
                      simpleType( model.type )
                font: ui.font.body1
                color: ui.color.secondaryTextFrom( root.color )
        }

        Lp.FactionIcon {
                id: factionIcon

                anchors {
                        verticalCenter: typeAndSubtype.verticalCenter
                        left: typeAndSubtype.right

                        leftMargin: 4 * ui.dp
                }

                faction: model.faction
        }

        Column {
                id: primaryContent

                width: 248 * ui.dp

                anchors {
                        top: root.top
                        left: root.left

                        topMargin: 72 * ui.dp
                        leftMargin: 104 * ui.dp
                }

                spacing: 16 * ui.dp

                Lp.CardStats {
                        id: cardStats
                }

                Text {
                        id: text

                        anchors {
                                left: primaryContent.left
                                right: primaryContent.right
                        }

                        text: model.text
                        font: ui.font.body1
                        color: ui.color.textFrom( root.color )
                        textFormat: Text.RichText
                        wrapMode: Text.Wrap
                }

                Lp.InfluenceValue {
                        id: influenceValue

                        anchors {
                                left: primaryContent.left
                        }

                        count: model.influenceValue
                        color: ui.color.faction( model.faction )
                }

                Row {
                        id: buttons

                        anchors {
                                left: primaryContent.left

                                leftMargin: -8 * ui.dp
                        }

                        spacing: 8 * ui.dp

                        Lp.FlatButton {
                                id: removeButton

                                text: qsTr( "Remove" )

                                onClicked: {
                                        if ( ( model.type ===
                                               "Corporation Identity" ) ||
                                             ( model.type ===
                                               "Runner Identity" ) ) {
                                                /* */
                                        } else {
                                                lpDeck.remove( 1,
                                                   lpDeck.card( model.title ) );
                                        }
                                }
                        }

                        Lp.FlatButton {
                                id: addButton

                                text: qsTr( "Add" )

                                onClicked: {
                                        if ( ( model.type ===
                                               "Corporation Identity" ) ||
                                             ( model.type ===
                                               "Runner Identity" ) ) {
                                                lpDeck.add( 1, lpDatabase.card(
                                                        model.title,
                                                        model.subtitle ) );
                                        } else {
                                                lpDeck.add( 1,
                                               lpDatabase.card( model.title ) );
                                        }
                                }
                        }
                }
        }

        Column {
                id: secondaryContent

                height: childrenRect.height

                anchors {
                        top: root.top
                        left: root.left
                        right: root.right

                        topMargin: 72 * ui.dp
                        leftMargin: 384 * ui.dp
                        rightMargin: 16 * ui.dp
                }

                spacing: 8 * ui.dp

                // TOFIX: dirty hack to fake card stats height
                Item {
                        id: fakeItem

                        width: height
                        height: 32 * ui.dp
                }

                Text {
                        id: flavorText

                        anchors {
                                left: secondaryContent.left
                                right: secondaryContent.right
                        }

                        visible: model.flavorText !== ""

                        text: model.flavorText
                        font: ui.font.body1
                        color: ui.color.secondaryTextFrom( root.color )
                        wrapMode: Text.Wrap
                }

                Text {
                        id: set

                        anchors {
                                left: secondaryContent.left
                                right: secondaryContent.right
                        }

                        text: model.set + " • " + model.number
                        font: ui.font.body1
                        color: ui.color.secondaryTextFrom( root.color )
                        wrapMode: Text.Wrap
                }

                Text {
                        id: dataPack

                        anchors {
                                left: secondaryContent.left
                                right: secondaryContent.right
                        }

                        visible: model.dataPack !== ""

                        text: "Data Pack: " + model.dataPack
                        font: ui.font.body1
                        color: ui.color.secondaryTextFrom( root.color )
                        wrapMode: Text.Wrap
                }

                Text {
                        id: illustrator

                        anchors {
                                left: secondaryContent.left
                                right: secondaryContent.right
                        }

                        visible: model.illustrator !== ""

                        text: "Illustrator: " + model.illustrator
                        font: ui.font.body1
                        color: ui.color.secondaryTextFrom( root.color )
                        wrapMode: Text.Wrap
                }

                Text {
                        id: copyright

                        anchors {
                                left: secondaryContent.left
                                right: secondaryContent.right
                        }

                        text: model.copyright
                        font: ui.font.body1
                        color: ui.color.secondaryTextFrom( root.color )
                        wrapMode: Text.Wrap
                }
        }

        MouseArea {
                id: mouseArea

                height: 72 * ui.dp

                anchors {
                        top: root.top
                        left: root.left
                        right: root.right
                }

                onClicked: {
                        database.forceActiveFocus( Qt.MouseFocusReason );
                        listView.currentIndex = index;
                }
        }

        Behavior on height {
                NumberAnimation {
                        easing.type: Easing.OutCubic
                }
        }

        Behavior on color {
                ColorAnimation {
                        easing.type: Easing.OutCubic
                }
        }
}
