// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

FocusScope {
        id: root

        width: searchRole.width + dropDownIcon.width
        height: 64 * ui.dp

        property color backgroundColor: "#000000"

        Text {
                id: searchRole

                anchors {
                        top: root.top
                        right: dropDownIcon.left

                        topMargin: 20 * ui.dp
                }

                text: filter.searchRole
                font: ui.font.title
                color: ui.color.textFrom( root.backgroundColor )
        }

        Lp.Icon {
                id: dropDownIcon

                anchors {
                        verticalCenter: root.verticalCenter
                        right: root.right
                }

                image: ui.image.dropDown
                color: ui.color.iconFrom( root.backgroundColor )
        }

        MouseArea {
                id: mouseArea

                anchors {
                        fill: root

                        topMargin: 8 * ui.dp
                        bottomMargin: 8 * ui.dp
                        leftMargin: -12 * ui.dp
                        rightMargin: -12 * ui.dp
                }

                onClicked: menu.show()
        }

        Lp.Menu {
                id: menu

                width: 48 * ui.dp * 2.5

                anchors {
                        top: root.top
                        right: root.right

                        topMargin: 8 * ui.dp
                        rightMargin: -12 * ui.dp
                }

                items: [
                        Lp.MenuItem {
                                name: qsTr( "Subtype" )
                                width: menu.width
                                onClicked: {
                                        filter.searchBy( "Subtype" )
                                        menu.hide()
                                }
                        },
                        Lp.MenuItem {
                                name: qsTr( "Text" )
                                width: menu.width
                                onClicked: {
                                        filter.searchBy( "Text" )
                                        menu.hide()
                                }
                        },
                        Lp.MenuItem {
                                name: qsTr( "Title" )
                                width: menu.width
                                onClicked: {
                                        filter.searchBy( "Title" )
                                        menu.hide()
                                }
                        }
                ]
        }
}
