// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

Lp.Paper {
        id: root

        implicitWidth: 64 * ui.dp * 8
        implicitHeight: 480 * ui.dp

        shadow: true

        Lp.ToolBar {
                id: toolBar

                z: 1

                anchors {
                        top: root.top
                        left: root.left
                        right: root.right
                }

                title: "Deck"
                color: lpDeck.faction === "" ?
                       ui.color.toolBar :
                       ui.color.faction( lpDeck.faction )

                Lp.ActionIcon {
                        id: sortIcon

                        anchors {
                                verticalCenter: toolBar.verticalCenter
                                right: moreIcon.left

                                rightMargin: 24 * ui.dp
                        }

                        image: ui.image.sort
                        color: ui.color.iconFrom( toolBar.color )

                        onClicked: sortMenu.show()
                }

                Lp.DeckSortMenu {
                        id: sortMenu

                        anchors {
                                top: toolBar.top
                                right: sortIcon.right

                                topMargin: 8 * ui.dp
                                rightMargin: -12 * ui.dp
                        }
                }

                Lp.ActionIcon {
                        id: moreIcon

                        anchors {
                                verticalCenter: toolBar.verticalCenter
                                right: toolBar.right

                                rightMargin: 20 * ui.dp
                        }

                        image: ui.image.more
                        color: ui.color.iconFrom( toolBar.color )

                        onClicked: moreMenu.show()
                }

                Lp.DeckMoreMenu {
                        id: moreMenu

                        anchors {
                                top: toolBar.top
                                right: toolBar.right

                                topMargin: 8 * ui.dp
                                rightMargin: 8 * ui.dp
                        }
                }
        }

        ListView {
                id: listView

                function getProperty( string ) {
                        switch ( string ) {
                                case "Type":
                                        return "extendedType"
                                default:
                                        return string.toLowerCase()
                        }
                }

                anchors {
                        top: toolBar.bottom
                        bottom: root.bottom
                        left: root.left
                        right: root.right
                }

                focus: true
                clip: true

                model: lpDeck.sorted
                delegate: delegate
                section {
                        //property: "extendedType"
                        //delegate: sectionDelegate
                        property: getProperty( deckSorter.role )
                        delegate: sectionDelegate
                        criteria: ( section.property === "title" ) ?
                                  ViewSection.FirstCharacter :
                                  ViewSection.FullString
                }

                bottomMargin: 8 * ui.dp
                boundsBehavior: Flickable.StopAtBounds

                Keys.onPressed: {
                        if ( count === 0 ) {
                                return;
                        }
                        switch ( event.key ) {
                        case Qt.Key_Home:
                                currentIndex = 0;
                                positionViewAtBeginning();
                                event.accepted = true;
                                break;
                        case Qt.Key_End:
                                currentIndex = count - 1;
                                positionViewAtEnd();
                                event.accepted = true;
                                break;
                        case Qt.Key_J:
                        case Qt.Key_Down:
                                incrementCurrentIndex();
                                positionViewAtIndex( currentIndex,
                                        ListView.Center);
                                event.accepted = true;
                                break;
                        case Qt.Key_K:
                        case Qt.Key_Up:
                                decrementCurrentIndex();
                                positionViewAtIndex( currentIndex,
                                        ListView.Center);
                                event.accepted = true;
                                break;
                        case Qt.Key_Enter:
                        case Qt.Key_Return:
                                lpDeck.add( 1, lpDeck.card(
                                        currentItem.title ) );
                                event.accepted = true;
                                break;
                        case Qt.Key_Backspace:
                        case Qt.Key_Delete:
                                lpDeck.remove( 1, lpDeck.card(
                                        currentItem.title ) );
                                event.accepted = true;
                                break;
                        case Qt.Key_1:
                                lpDeck.setQuantity( 1, lpDeck.card(
                                        currentItem.title ) );
                                event.accepted = true;
                                break;
                        case Qt.Key_2:
                                lpDeck.setQuantity( 2, lpDeck.card(
                                        currentItem.title ) );
                                event.accepted = true;
                                break;
                        case Qt.Key_3:
                                lpDeck.setQuantity( 3, lpDeck.card(
                                        currentItem.title ) );
                                event.accepted = true;
                                break;
                        }
                }
        }

        Component {
                id: delegate

                Lp.DeckItem {
                        id: deckItem

                        width: listView.width
                }
        }

        Component {
                id: sectionDelegate

                Item {
                        id: sectionDelegateItem

                        width: parent.width
                        height: 72 * ui.dp

                        Text {
                                id: sectionProperty

                                anchors {
                                        top: sectionDelegateItem.top
                                        left: sectionDelegateItem.left

                                        topMargin: 20 * ui.dp
                                        leftMargin: 32 * ui.dp
                                }

                                text: section
                                font: ui.font.subhead
                                color: ui.color.secondaryText
                        }

                        Lp.Divider {
                                id: divider

                                anchors {
                                        top: sectionDelegateItem.top
                                        left: sectionDelegateItem.left
                                        right: sectionDelegateItem.right

                                        topMargin: -height
                                }

                                color: ui.color.dividerFrom( root.color )
                        }
                }
        }
}
