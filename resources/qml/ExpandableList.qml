// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

FocusScope {
        id: root

        property alias title: titleTile.text
        property alias tiles: tilesPositioner.children

        implicitWidth: 336 * ui.unit
        state: "Collapsed"
        clip: true

        Lp.ListTile {
                id: titleTile

                z: 1

                anchors {
                  top: root.top
                  left: root.left
                  right: root.right
                }

                onClicked: root.state = ( root.state === "Collapsed" ) ?
                  "Expanded" : "Collapsed"
        }

        Column {
                id: tilesPositioner

                anchors {
                  bottom: root.bottom
                  left: root.left
                  right: root.right
                }
        }

        Lp.Divider {
                id: bottomDivider

                z: 1

                anchors {
                  bottom: root.bottom
                  left: root.left
                  right: root.right
                }
        }

        Behavior on height {
          NumberAnimation {
            duration: 100
            easing.type: Easing.OutCubic
          }
        }

        states: [

          State {
            name: "Collapsed"

            PropertyChanges {
              target: root
              height: titleTile.height
            }
            PropertyChanges {
              target: titleTile
              rightImage: ui.image.expandMore
            }
          },

          State {
            name: "Expanded"

            PropertyChanges {
              target: root
              height: titleTile.height + tilesPositioner.height
            }
            PropertyChanges {
              target: titleTile
              rightImage: ui.image.expandLess
            }
          }
        ]
}
