// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import QtGraphicalEffects 1.0

FocusScope {
        id: root

        property string faction: "Neutral Corporation"

        width: 13 * ui.dp
        height: width

        Image {
                id: icon

                anchors {
                        fill: root
                }

                visible: false

                asynchronous: true
                source: switch ( root.faction ) {
                        case "Haas-Bioroid":
                        return ui.image.haasBioroid;
                        case "Jinteki":
                        return ui.image.jinteki;
                        case "NBN":
                        return ui.image.nbn;
                        case "Weyland Consortium":
                        return ui.image.weylandConsortium;
                        case "Neutral Corporation":
                        return ui.image.corporation;
                        case "Anarch":
                        return ui.image.anarch;
                        case "Criminal":
                        return ui.image.criminal;
                        case "Shaper":
                        return ui.image.shaper;
                        case "Neutral Runner":
                        return ui.image.runner;
                        default:
                        return ui.image.corporation;
                }
                sourceSize {
                        width: root.width
                        height: root.height
                }
        }

        ColorOverlay {
                id: coloredIcon

                anchors {
                        fill: root
                }

                opacity: color.a

                source: icon
                color: ui.color.faction( root.faction )
        }
}
