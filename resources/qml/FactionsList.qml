// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

Lp.ExpandableList {
  id: root

  implicitWidth: 336 * ui.dp

  title: "Faction"

  tiles: [

    Lp.ToggleListTile {
      id: haasBioroidTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.corporation.isOn

      text: "Haas-Bioroid"
      leftImage: ui.image.haasBioroid
      isOn: filter.haasBioroid.isOn

      onClicked: filter.toggleHaasBioroid()
    },

    Lp.ToggleListTile {
      id: jintekiTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.corporation.isOn

      text: "Jinteki"
      leftImage: ui.image.jinteki
      isOn: filter.jinteki.isOn

      onClicked: filter.toggleJinteki()
    },

    Lp.ToggleListTile {
      id: nbnTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.corporation.isOn

      text: "NBN"
      isOn: filter.nbn.isOn
      leftImage: ui.image.nbn

      onClicked: filter.toggleNbn()
    },

    Lp.ToggleListTile {
      id: weylandConsortiumTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.corporation.isOn

      text: "Weyland Consortium"
      leftImage: ui.image.weylandConsortium
      isOn: filter.weylandConsortium.isOn

      onClicked: filter.toggleWeylandConsortium()
    },

    Lp.ToggleListTile {
      id: neutralCorporationTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.corporation.isOn

      text: filter.runner.isOff ? "Neutral" : "Neutral Corporation"
      leftImage: ui.image.corporation
      isOn: filter.neutralCorporation.isOn

      onClicked: filter.toggleNeutralCorporation()
    },

    Lp.ToggleListTile {
      id: anarchTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.runner.isOn

      text: "Anarch"
      leftImage: ui.image.anarch
      isOn: filter.anarch.isOn

      onClicked: filter.toggleAnarch()
    },

    Lp.ToggleListTile {
      id: criminalTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.runner.isOn

      text: "Criminal"
      leftImage: ui.image.criminal
      isOn: filter.criminal.isOn

      onClicked: filter.toggleCriminal()
    },

    Lp.ToggleListTile {
      id: shaperTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.runner.isOn

      text: "Shaper"
      leftImage: ui.image.shaper
      isOn: filter.shaper.isOn

      onClicked: filter.toggleShaper()
    },

    Lp.ToggleListTile {
      id: neutralRunnerTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.runner.isOn

      text: filter.corporation.isOff ? "Neutral" : "Neutral Runner"
      leftImage: ui.image.runner
      isOn: filter.neutralRunner.isOn

      onClicked: filter.toggleNeutralRunner()
    }
  ]
}

