// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

Lp.Paper {
        id: root

        implicitWidth: 64 * ui.dp * 5
        implicitHeight: 600 * ui.dp

        shadow: true

        Lp.ToolBar {
                id: toolBar

                z: 1

                anchors {
                        top: root.top
                        left: root.left
                        right: root.right
                }

                title: "Filters"
        }

        Flickable {
                id: flickableList

                clip: true

                anchors {
                        top: toolBar.bottom
                        bottom: root.bottom
                        left: root.left
                        right: root.right
                }

                contentHeight: filtersList.height
                topMargin: 8 * ui.dp
                boundsBehavior: Flickable.StopAtBounds

                Column {
                        id: filtersList

                        anchors {
                          left: flickableList.contentItem.left
                          right: flickableList.contentItem.right
                        }

                        Lp.SidesList {
                                id: sidesList

                                anchors {
                                  left: filtersList.left
                                  right: filtersList.right
                                }
                        }

                        Lp.FactionsList {
                                id: factionsList

                                anchors {
                                  left: filtersList.left
                                  right: filtersList.right
                                }
                        }

                        Lp.TypesList {
                                id: typesList

                                anchors {
                                  left: filtersList.left
                                  right: filtersList.right
                                }
                        }

                        Lp.SetsList {
                                id: setsList

                                anchors {
                                  left: filtersList.left
                                  right: filtersList.right
                                }
                        }
                }
        }

        Lp.Divider {
                id: divider

                anchors {
                        top: root.top
                        bottom: root.bottom
                        right: root.right
                }

                color: ui.color.dividerFrom( root.color )
        }
}
