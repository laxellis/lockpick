// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4

Item {
        id: root

        property alias text: text.text
        property alias color: text.color
        property bool disabled: false

        signal clicked()

        width: ( text.width + 16 * ui.dp > 64 * ui.dp ) ?
                 text.width + 16 * ui.dp : 64 * ui.dp
        height: 36 * ui.dp

        Rectangle {
                id: background

                anchors {
                        fill: root
                }

                color: "#999999"
                radius: 2 * ui.dp
        }

        Text {
                id: text

                anchors {
                        verticalCenter: root.verticalCenter
                        horizontalCenter: root.horizontalCenter
                }

                text: "Button"

                font: ui.font.button
                color: ui.color.primary
        }

        MouseArea {
                id: mouseArea

                anchors {
                        fill: root

                        topMargin: -6 * ui.dp
                        bottomMargin: -6 * ui.dp
                }

                hoverEnabled: true

                onClicked: {
                        if ( root.state !== "Disabled" ) {
                                root.clicked();
                        }
                }
        }

        states: [
                State {
                        name: "Normal"
                        when: ! mouseArea.containsMouse &&
                              ! root.disabled
                        PropertyChanges {
                                target: background
                                opacity: 0.0
                        }
                },
                State {
                        name: "Hover"
                        when: mouseArea.containsMouse &&
                              ! mouseArea.pressed &&
                              ! root.disabled
                        PropertyChanges {
                                target: background
                                opacity: 0.2
                        }
                },
                State {
                        name: "Pressed"
                        when: mouseArea.containsMouse &&
                              mouseArea.pressed &&
                              ! root.disabled
                        PropertyChanges {
                                target: background
                                opacity: 0.4
                        }
                },
                State {
                        name: "Disabled"
                        when: root.disabled
                        PropertyChanges {
                                target: text
                                color: ui.color.disabled
                        }
                        PropertyChanges {
                                target: background
                                opacity: 0.0
                        }
                }
        ]
}
