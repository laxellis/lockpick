// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import QtGraphicalEffects 1.0
import "." as Lp

Item {
        id: root

        property alias image: icon.image

        signal clicked()

        width: 56 * ui.dp
        height: width

        Rectangle {
                id: background

                anchors {
                        fill: root
                }

                radius: width / 2
                color: ui.color.faction( lpDeck.faction )

                layer {
                        enabled: true
                        effect: DropShadow {
                                source: background
                                fast: true
                                radius: 16 * ui.dp
                                verticalOffset: mouseArea.containsMouse ?
                                                8 * ui.dp : 2 * ui.dp
                                transparentBorder: true
                                color: ui.color.hintText
                        }
                }
        }

        Lp.Icon {
                id: icon

                anchors {
                        centerIn: root
                }

                color: ui.color.iconFrom( background.color )
        }

        MouseArea {
                id: mouseArea

                anchors {
                        fill: root
                }

                hoverEnabled: true

                onClicked: root.clicked()
        }
}
