// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

FocusScope {
        id: root

        property alias icon: icon.image
        property alias text: text.text

        width: icon.width + text.width + text.anchors.leftMargin
        height: icon.height

        Lp.Icon {
                id: icon

                anchors {
                        top: root.top
                        left: root.left
                }

                color: ui.color.iconFrom( ui.color.card )
        }

        Text {
                id: text

                anchors {
                        verticalCenter: root.verticalCenter
                        left: icon.right

                        leftMargin: 4 * ui.dp
                }

                font: ui.font.body1
                color: ui.color.textFrom( ui.color.card )
        }
}
