// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4

Item {
  id: root

  property alias color: ink.color
  property real dropSize: 24 * ui.dp

  function drop( x, y ) {
    ink.x = x - dropSize / 2
    ink.y = y - dropSize / 2
    dropAnimation.start()
  }
  function clear() {
    clearAnimation.start()
  }
  function hypot( x, y ) {
    return Math.sqrt( x * x, y * y )
  }

  implicitWidth: 288 * ui.dp
  implicitHeight: 48 * ui.dp

  Rectangle {
    id: ink

    width: dropSize
    height: width

    opacity: 0.0

    radius: width / 2
  }

  ParallelAnimation {
    id: dropAnimation

    NumberAnimation {
      target: ink
      property: "scale"
      from: 1.0
      to: 2 * hypot( root.width, root.height ) / root.dropSize + 1
      easing.type: Easing.OutCubic
    }
    NumberAnimation {
      target: ink
      property: "opacity"
      from: 0.0
      to: 1.0
      easing.type: Easing.OutCubic
    }
  }

  NumberAnimation {
    id: clearAnimation

    target: ink
    property: "opacity"
    to: 0.0
    easing.type: Easing.OutCubic
  }
}

