// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

FocusScope {
        id: root

        property alias text: text.text
        property alias leftImage: leftImage.image
        property alias leftImageColor: leftImage.color
        property alias rightImage: rightImage.image
        property alias rightImageColor: rightImage.color

        signal clicked()

        implicitWidth: 6 * height
        height: 56 * ui.dp

        Rectangle {
                id: background

                anchors {
                  fill: root
                }

                color: ui.color.card
        }

        Lp.Icon {
                id: leftImage

                width: 24 * ui.dp
                height: width

                anchors {
                  verticalCenter: root.verticalCenter
                  left: root.left

                  leftMargin: 24 * ui.dp
                }

                color: ui.color.iconFrom( background.color )
                visible: Qt.resolvedUrl( image ) !== ""
        }

        Text {
                id: text

                anchors {
                  baseline: root.bottom
                  left: root.left
                  right: rightImage.left

                  baselineOffset: -22 * ui.dp
                  leftMargin: Qt.resolvedUrl( leftImage.image ) === "" ?
                              24 * ui.dp : 80 * ui.dp
                  rightMargin: 24 * ui.dp
                }

                font: ui.font.subhead
                color: ui.color.textFrom( background.color )
                elide: Text.ElideRight
        }

        Lp.Icon {
                id: rightImage

                width: 24 * ui.dp
                height: width

                anchors {
                  verticalCenter: root.verticalCenter
                  right: root.right

                  rightMargin: 24 * ui.dp
                }

                visible: Qt.resolvedUrl( image ) !== ""
        }

        MouseArea {
                id: mouseArea

                anchors {
                  fill: root
                }

                onClicked: root.clicked()
        }
}
