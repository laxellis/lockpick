// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import QtQuick.Dialogs 1.2
import "." as Lp

Item {
        id: root

        implicitWidth: 800 * ui.dp
        implicitHeight: 600 * ui.dp

        Keys.onPressed: {
                if ( ( event.key === Qt.Key_F ) &&
                     ( event.modifiers === Qt.ControlModifier ) ) {
                        appBar.searchField.forceActiveFocus(
                                Qt.MouseFocusReason )
                        event.accepted = true;
                }
        }

        Rectangle {
                id: background

                anchors {
                        fill: root
                }

                color: "#e0e0e0"
        }

        Lp.AppBar {
                id: appBar

                z: 1

                anchors {
                        top: root.top
                        left: root.left
                        right: root.right
                }
        }

        Lp.Filters {
                id: filters

                anchors {
                        top: appBar.bottom
                        bottom: root.bottom
                        left: root.left

                        margins: 8 * ui.dp
                }
        }

        Lp.Database {
                id: database

                anchors {
                        top: appBar.bottom
                        bottom: root.bottom
                        left: filters.right
                        right: deck.left

                        margins: 8 * ui.dp
                }

                KeyNavigation.tab: deck

                Component.onCompleted: forceActiveFocus( Qt.OtherFocusReason )
        }

        Lp.Deck {
                id: deck

                anchors {
                        top: appBar.bottom
                        bottom: root.bottom
                        right: root.right

                        margins: 8 * ui.dp
                }

                KeyNavigation.tab: database
        }

        Lp.AboutDialog {
                id: aboutDialog

                z: 1

                anchors {
                        fill: root
                }
        }

        FileDialog {
                id: openFileDialog

                title: qsTr( "Open deck" )
                nameFilters: [ qsTr( "Deck files (*.lpd *.o8d)" ) ]

                onAccepted: lpDeck.open( fileUrl )
        }

        FileDialog {
                id: saveAsFileDialog

                title: qsTr( "Save deck" )
                selectExisting: false
                nameFilters: [ qsTr( "Lockpick deck (*.lpd)" ) ]

                onAccepted: lpDeck.saveAs( fileUrl )
        }

        //FileDialog {
        //        id: importOctgnDeckFileDialog

        //        title: qsTr( "Import deck" )
        //        nameFilters: [ qsTr( "OCTGN deck (*.o8d)" ) ]

        //        onAccepted: lpDeck.importOctgnDeck( fileUrl )
        //}

        FileDialog {
                id: exportAsOctgnDeckFileDialog

                title: qsTr( "Export deck" )
                selectExisting: false
                nameFilters: [ qsTr( "OCTGN deck (*.o8d)" ) ]

                onAccepted: lpDeck.exportAsOctgnDeck( fileUrl )
        }

        Connections {
                target: lpDeck
                onOpenSaveAsFileDialog: saveAsFileDialog.open()
                onOpenExportAsOctgnDeckFileDialog:
                                              exportAsOctgnDeckFileDialog.open()
        }
}
