// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

Lp.Paper {
        id: root

        property alias items: items.children

        function show() {
                root.state = "visible"
        }

        function hide() {
                root.state = "hidden"
        }

        implicitWidth: 48 * ui.dp * 4

        state: "hidden"

        shadow: true

        Behavior on height {
                NumberAnimation {
                        duration: 100
                        easing.type: Easing.OutCubic
                }
        }

        Column {
                id: items

                anchors {
                        fill: root

                        topMargin: 8 * ui.dp
                        bottomMargin: 8 * ui.dp
                }

                clip: true
        }

        states: [
                State {
                        name: "visible"
                        PropertyChanges {
                                target: root
                                visible: true
                                height: root.items.length * 48 * ui.dp
                                        + 8 * ui.dp * 2
                        }
                },
                State {
                        name: "hidden"
                        PropertyChanges {
                                target: root
                                visible: false
                                height: 48 * ui.dp
                        }
                }
        ]
}
