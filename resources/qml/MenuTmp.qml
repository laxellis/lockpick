// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

Lp.Paper {
        id: root

        property string choice: ""
        property var choices: []

        width: 52 * ui.dp * 3
        height: choices.length * 52 * ui.dp + choicesList.topMargin
                + choicesList.bottomMargin

        visible: activeFocus

        shadow: true

        Keys.onEscapePressed: focus = false

        ListModel {
                id: choicesModel
        }

        ListView {
                id: choicesList

                anchors {
                        fill: root
                }

                model: choicesModel
                delegate: choiceDelegate

                topMargin: 8 * ui.dp
                bottomMargin: 8 * ui.dp
        }

        Component {
                id: choiceDelegate

                Item {
                        id: container

                        height: 52 * ui.dp

                        anchors {
                                left: parent.left
                                right: parent.right
                        }

                        Text {
                                anchors {
                                        baseline: container.bottom
                                        left: container.left

                                        baselineOffset: -20 * ui.dp
                                        leftMargin: 24 * ui.dp
                                }

                                text: name
                                font: ui.font.title
                                color: ( name === choice ) ?
                                        ui.color.accent : ui.color.text
                        }

                        MouseArea {
                                id: mouseArea

                                anchors {
                                        fill: container
                                }

                                onClicked: {
                                        choice = name
                                        root.focus = false
                                }
                        }
                }
        }

        Behavior on height {
                NumberAnimation {
                        easing.type: Easing.OutCubic
                }
        }

        Component.onCompleted: {
                var count = choices.length
                for ( var i = 0; i < count; i++ ) {
                        choicesModel.append( { "name": choices[ i ] } )
                }
        }
}
