// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

Lp.Paper {
        id: root

        property alias text: textInput.text

        signal editingFinished()

        function show() {
                root.state = "visible"
                root.forceActiveFocus( Qt.MouseFocusReason )
        }

        function hide() {
                root.state = "hidden"
        }

        implicitWidth: height * 6
        height: 64 * ui.dp

        state: "hidden"

        color: ui.color.toolBar

        Lp.ActionIcon {
                id: backIcon

                anchors {
                        verticalCenter: root.verticalCenter
                        left: root.left

                        leftMargin: 20 * ui.dp
                }

                image: ui.image.arrowBack
                color: ui.color.iconFrom( root.color )

                onClicked: {
                        root.hide()
                        textInput.text = ""
                }
        }

        Text {
                id: label

                anchors {
                        top: root.top
                        left: backIcon.right
                        right: clearIcon.left

                        topMargin: 20 * ui.dp
                        leftMargin: 24 * ui.dp
                        rightMargin: 24 * ui.dp
                }

                visible: ! textInput.text

                text: qsTr( "Search" )
                font: ui.font.title
                color: ui.color.hintTextFrom( root.color )
        }

        TextInput {
                id: textInput

                anchors {
                        fill: label
                }

                clip: true
                focus: true

                font: ui.font.title
                color: ui.color.textFrom( root.color )

                onEditingFinished: root.editingFinished()
                Keys.onEscapePressed: root.editingFinished()
        }

        Lp.ActionIcon {
                id: clearIcon

                anchors {
                        verticalCenter: root.verticalCenter
                        right: databaseSortRoleMenu.left

                        rightMargin: 20 * ui.dp
                }

                visible: textInput.text

                image: ui.image.clear
                color: ui.color.iconFrom( root.color )

                onClicked: textInput.text = ""
        }

        Lp.DatabaseSortRoleMenu {
                id: databaseSortRoleMenu

                anchors {
                        top: root.top
                        right: root.right

                        rightMargin: 20 * ui.dp
                }
        }

        states: [
                State {
                        name: "visible"
                        PropertyChanges {
                                target: root
                                visible: true
                        }
                },
                State {
                        name: "hidden"
                        PropertyChanges {
                                target: root
                                visible: false
                        }
                }
        ]
}
