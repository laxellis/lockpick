// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

Lp.ExpandableList {
  id: root

  implicitWidth: 336 * ui.dp

  title: "Set"

  tiles: [

    Lp.ToggleListTile {
      id: coreSetTile

      anchors {
        left: parent.left
        right: parent.right
      }

      text: "Core Set"
      leftImage: ui.image.coreSet
      isOn: filter.coreSet.isOn

      onClicked: filter.toggleCoreSet()
    },

    Lp.ToggleListTile {
      id: genesisTile

      anchors {
        left: parent.left
        right: parent.right
      }

      text: "Genesis"
      leftImage: ui.image.genesis
      isOn: filter.genesis.isOn

      onClicked: filter.toggleGenesis()
    },

    Lp.ToggleListTile {
      id: creationAndControlTile

      anchors {
        left: parent.left
        right: parent.right
      }

      text: "Creation and Control"
      leftImage: ui.image.creationAndControl
      isOn: filter.creationAndControl.isOn

      onClicked: filter.toggleCreationAndControl()
    },

    Lp.ToggleListTile {
      id: spinTile

      anchors {
        left: parent.left
        right: parent.right
      }

      text: "Spin"
      leftImage: ui.image.spin
      isOn: filter.spin.isOn

      onClicked: filter.toggleSpin()
    },

    Lp.ToggleListTile {
      id: honorAndProfitTile

      anchors {
        left: parent.left
        right: parent.right
      }

      text: "Honor and Profit"
      leftImage: ui.image.honorAndProfit
      isOn: filter.honorAndProfit.isOn

      onClicked: filter.toggleHonorAndProfit()
    },

    Lp.ToggleListTile {
      id: lunarTile

      anchors {
        left: parent.left
        right: parent.right
      }

      text: "Lunar"
      leftImage: ui.image.lunar
      isOn: filter.lunar.isOn

      onClicked: filter.toggleLunar()
    },

    Lp.ToggleListTile {
            id: orderAndChaosTile

            anchors {
                    left: parent.left
                    right: parent.right
            }

            text: "Order and Chaos"
            leftImage: ui.image.orderAndChaos
            isOn: filter.orderAndChaos.isOn

            onClicked: filter.toggleOrderAndChaos()
    }
  ]
}

