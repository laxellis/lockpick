// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

import QtQuick 2.4
import "." as Lp

Lp.ExpandableList {
  id: root

  implicitWidth: 336 * ui.dp

  title: "Type"

  tiles: [

    Lp.ToggleListTile {
      id: corporationIdentityTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.corporation.isOn

      text: filter.runner.isOff ? "Identity" : "Corporation Identity"
      leftImage: ui.image.corporationIdentity
      isOn: filter.corporationIdentity.isOn

      onClicked: filter.toggleCorporationIdentity()
    },

    Lp.ToggleListTile {
      id: iceTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.corporation.isOn

      text: "Ice"
      leftImage: ui.image.ice
      isOn: filter.ice.isOn

      onClicked: filter.toggleIce()
    },

    Lp.ToggleListTile {
      id: agendaTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.corporation.isOn

      text: "Agenda"
      leftImage: ui.image.agenda
      isOn: filter.agenda.isOn

      onClicked: filter.toggleAgenda()
    },

    Lp.ToggleListTile {
      id: assetTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.corporation.isOn

      text: "Asset"
      leftImage: ui.image.asset
      isOn: filter.asset.isOn

      onClicked: filter.toggleAsset()
    },

    Lp.ToggleListTile {
      id: upgradeTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.corporation.isOn

      text: "Upgrade"
      leftImage: ui.image.upgrade
      isOn: filter.upgrade.isOn

      onClicked: filter.toggleUpgrade()
    },

    Lp.ToggleListTile {
      id: operationTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.corporation.isOn

      text: "Operation"
      leftImage: ui.image.operation
      isOn: filter.operation.isOn

      onClicked: filter.toggleOperation()
    },

    Lp.ToggleListTile {
      id: runnerIdentityTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.runner.isOn

      text: filter.corporation.isOff ? "Identity" : "Runner Identity"
      leftImage: ui.image.runnerIdentity
      isOn: filter.runnerIdentity.isOn

      onClicked: filter.toggleRunnerIdentity()
    },

    Lp.ToggleListTile {
      id: programTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.runner.isOn

      text: "Program"
      leftImage: ui.image.program
      isOn: filter.program.isOn

      onClicked: filter.toggleProgram()
    },

    Lp.ToggleListTile {
      id: hardwareTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.runner.isOn

      text: "Hardware"
      leftImage: ui.image.hardware
      isOn: filter.hardware.isOn

      onClicked: filter.toggleHardware()
    },

    Lp.ToggleListTile {
      id: resourceTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.runner.isOn

      text: "Resource"
      leftImage: ui.image.resource
      isOn: filter.resource.isOn

      onClicked: filter.toggleResource()
    },

    Lp.ToggleListTile {
      id: eventTile

      anchors {
        left: parent.left
        right: parent.right
      }

      visible: filter.runner.isOn

      text: "Event"
      leftImage: ui.image.event
      isOn: filter.eventCard.isOn

      onClicked: filter.toggleEvent()
    }
  ]
}

