// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "lockpick/card.hpp"

namespace lp {

Card::Card( QObject *parent )
    : QObject( parent )
    , m_advancementRequirement( 0 )
    , m_agendaPoints( 0 )
    , m_baseLink( 0 )
    , m_copyright()
    , m_dataPack()
    , m_extendedType()
    , m_extendedTypeNumber( 0 )
    , m_faction()
    , m_factionNumber( 0 )
    , m_flavorText()
    , m_guid()
    , m_illustrator()
    , m_influenceLimit( 15 )
    , m_influenceValue( 0 )
    , m_installCost( 0 )
    , m_isUnique( false )
    , m_memoryCost( 0 )
    , m_minimumDeckSize( 45 )
    , m_number( 0 )
    , m_playCost( 0 )
    , m_quantity( 1 )
    , m_rawText()
    , m_rezCost( 0 )
    , m_side()
    , m_sideNumber( 0 )
    , m_set()
    , m_setNumber( 0 )
    , m_strength( 0 )
    , m_subtitle()
    , m_subtype()
    , m_text()
    , m_title()
    , m_trashCost( 0 )
    , m_type()
    , m_typeNumber( 0 )
{
}

Card::Card( Card *card, QObject *parent )
    : QObject( parent )
    , m_advancementRequirement( card->advancementRequirement() )
    , m_agendaPoints( card->agendaPoints() )
    , m_baseLink( card->baseLink() )
    , m_copyright( card->copyright() )
    , m_dataPack( card->dataPack() )
    , m_extendedType( card->extendedType() )
    , m_extendedTypeNumber( card->extendedTypeNumber() )
    , m_faction( card->faction() )
    , m_factionNumber( card->factionNumber() )
    , m_flavorText( card->flavorText() )
    , m_guid( card->guid() )
    , m_illustrator( card->illustrator() )
    , m_influenceLimit( card->influenceLimit() )
    , m_influenceValue( card->influenceValue() )
    , m_installCost( card->installCost() )
    , m_isUnique( card->isUnique() )
    , m_memoryCost( card->memoryCost() )
    , m_minimumDeckSize( card->minimumDeckSize() )
    , m_number( card->number() )
    , m_playCost( card->playCost() )
    , m_quantity( card->quantity() )
    , m_rawText( card->rawText() )
    , m_rezCost( card->rezCost() )
    , m_side( card->side() )
    , m_sideNumber( card->sideNumber() )
    , m_set( card->set() )
    , m_setNumber( card->setNumber() )
    , m_strength( card->strength() )
    , m_subtitle( card->subtitle() )
    , m_subtype( card->subtype() )
    , m_text( card->text() )
    , m_title( card->title() )
    , m_trashCost( card->trashCost() )
    , m_type( card->type() )
    , m_typeNumber( card->typeNumber() )
{
}

void Card::setAdvancementRequirement( const int &advancementRequirement )
{
        if ( advancementRequirement not_eq m_advancementRequirement ) {
                m_advancementRequirement = advancementRequirement;
        }
}

void Card::setAgendaPoints( const int &agendaPoints )
{
        if ( agendaPoints not_eq m_agendaPoints ) {
                m_agendaPoints = agendaPoints;
        }
}

void Card::setBaseLink( const int &baseLink )
{
        if ( baseLink not_eq m_baseLink ) {
                m_baseLink = baseLink;
        }
}

void Card::setCopyright( const QString &copyright )
{
        if ( copyright not_eq m_copyright ) {
                m_copyright = copyright;
        }
}

void Card::setDataPack( const QString &dataPack )
{
        if ( dataPack not_eq m_dataPack ) {
                m_dataPack = dataPack;
        }
}

void Card::setExtendedType( const QString &extendedType )
{
        if ( extendedType not_eq m_extendedType ) {
                m_extendedType = extendedType;
        }
}

void Card::setExtendedTypeNumber( const int &extendedTypeNumber )
{
        if ( extendedTypeNumber not_eq m_extendedTypeNumber ) {
                m_extendedTypeNumber = extendedTypeNumber;
        }
}

void Card::setFaction( const QString &faction )
{
        if ( faction not_eq m_faction ) {
                m_faction = faction;
        }
}

void Card::setFactionNumber( const int &factionNumber )
{
        if ( factionNumber not_eq m_factionNumber ) {
                m_factionNumber = factionNumber;
        }
}

void Card::setFlavorText( const QString &flavorText )
{
        if ( flavorText not_eq m_flavorText ) {
                m_flavorText = flavorText;
        }
}

void Card::setGuid( const QString &guid )
{
        if ( guid not_eq m_guid ) {
                m_guid = guid;
        }
}

void Card::setIllustrator( const QString &illustrator )
{
        if ( illustrator not_eq m_illustrator ) {
                m_illustrator = illustrator;
        }
}

void Card::setInfluenceLimit( const int &influenceLimit )
{
        if ( influenceLimit not_eq m_influenceLimit ) {
                m_influenceLimit = influenceLimit;
        }
}

void Card::setInfluenceValue( const int &influenceValue )
{
        if ( influenceValue not_eq m_influenceValue ) {
                m_influenceValue = influenceValue;
        }
}

void Card::setInstallCost( const int &installCost )
{
        if ( installCost not_eq m_installCost ) {
                m_installCost = installCost;
        }
}

void Card::setIsUnique( const bool &isUnique )
{
        if ( isUnique not_eq m_isUnique ) {
                m_isUnique = isUnique;
        }
}

void Card::setMemoryCost( const int &memoryCost )
{
        if ( memoryCost not_eq m_memoryCost ) {
                m_memoryCost = memoryCost;
        }
}

void Card::setMinimumDeckSize( const int &minimumDeckSize )
{
        if ( minimumDeckSize not_eq m_minimumDeckSize ) {
                m_minimumDeckSize = minimumDeckSize;
        }
}

void Card::setNumber( const int &number )
{
        if ( number not_eq m_number ) {
                m_number = number;
        }
}

void Card::setPlayCost( const int &playCost )
{
        if ( playCost not_eq m_playCost ) {
                m_playCost = playCost;
        }
}

void Card::setQuantity( const int &quantity )
{
        if ( quantity not_eq m_quantity ) {
                m_quantity = ( quantity <= 3 ) ? quantity : 3;
                emit dataChanged();
        }
}

void Card::setRawText( const QString &rawText )
{
        if ( rawText not_eq m_rawText ) {
                m_rawText = rawText;
        }
}

void Card::setRezCost( const int &rezCost )
{
        if ( rezCost not_eq m_rezCost ) {
                m_rezCost = rezCost;
        }
}

void Card::setSide( const QString &side )
{
        if ( side not_eq m_side ) {
                m_side = side;
        }
}

void Card::setSideNumber( const int &sideNumber )
{
        if ( sideNumber not_eq m_sideNumber ) {
                m_sideNumber = sideNumber;
        }
}

void Card::setSet( const QString &set )
{
        if ( set not_eq m_set ) {
                m_set = set;
        }
}

void Card::setSetNumber( const int &setNumber )
{
        if ( setNumber not_eq m_setNumber ) {
                m_setNumber = setNumber;
        }
}

void Card::setStrength( const int &strength )
{
        if ( strength not_eq m_strength ) {
                m_strength = strength;
        }
}

void Card::setSubtitle( const QString &subtitle )
{
        if ( subtitle not_eq m_subtitle ) {
                m_subtitle = subtitle;
        }
}

void Card::setSubtype( const QString &subtype )
{
        if ( subtype not_eq m_subtype ) {
                m_subtype = subtype;
        }
}

void Card::setText( const QString &primaryColor, const bool &isDark )
{
        m_text = m_rawText;
        QString luminance =
            isDark ? QLatin1String( "dark" ) : QLatin1String( "light" );
        m_text.replace( QLatin1String( "<b>" ),
                        QLatin1String( "<span style=\"color: " ) +
                            primaryColor + QLatin1String( "\">" ) )
            .replace( QLatin1String( "</b>" ), QLatin1String( "</span>" ) )
            .replace( QLatin1String( "{{credit}}" ),
                      QLatin1String( "<img src=\"/images/ui/small_credit_" ) +
                          luminance +
                          QLatin1String( ".svg\" width=8 height=13 />" ) )
            .replace( QLatin1String( "{{click}}" ),
                      QLatin1String( "<img src=\"/images/ui/small_click_" ) +
                          luminance +
                          QLatin1String( ".svg\" width=14 height=13 />" ) )
            .replace( QLatin1String( "{{recurring credit}}" ),
                      QLatin1String(
                          "<img src=\"/images/ui/small_recurring_credit_" ) +
                          luminance +
                          QLatin1String( ".svg\" width=12 height=13 />" ) )
            .replace( QLatin1String( "{{link}}" ),
                      QLatin1String( "<img src=\"/images/ui/small_link_" ) +
                          luminance +
                          QLatin1String( ".svg\" width=13 height=13 />" ) )
            .replace(
                 QLatin1String( "{{memory unit}}" ),
                 QLatin1String( "<img src=\"/images/ui/small_memory_unit_" ) +
                     luminance +
                     QLatin1String( ".svg\" width=14 height=13 />" ) )
            .replace(
                 QLatin1String( "{{subroutine}}" ),
                 QLatin1String( "<img src=\"/images/ui/small_subroutine_" ) +
                     luminance +
                     QLatin1String( ".svg\" width=12 height=13 />" ) )
            .replace( QLatin1String( "{{trash}}" ),
                      QLatin1String( "<img src=\"/images/ui/small_trash_" ) +
                          luminance +
                          QLatin1String( ".svg\" width=11 height=13 />" ) );

        emit dataChanged();
}

void Card::setTitle( const QString &title )
{
        if ( title not_eq m_title ) {
                m_title = title;
        }
}

void Card::setTrashCost( const int &trashCost )
{
        if ( trashCost not_eq m_trashCost ) {
                m_trashCost = trashCost;
        }
}

void Card::setType( const QString &type )
{
        if ( type not_eq m_type ) {
                m_type = type;
        }
}

void Card::setTypeNumber( const int &typeNumber )
{
        if ( typeNumber not_eq m_typeNumber ) {
                m_typeNumber = typeNumber;
        }
}

} // namespace lp
