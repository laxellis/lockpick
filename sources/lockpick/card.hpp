// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef LP_CARD_HPP
#define LP_CARD_HPP

#include <QtCore/QObject>

namespace lp {

class Card : public QObject {
        Q_OBJECT

      public:
        explicit Card( QObject *parent = 0 );
        explicit Card( Card *card, QObject *parent = 0 );

        int advancementRequirement() const;
        void setAdvancementRequirement( const int &advancementRequirement );

        int agendaPoints() const;
        void setAgendaPoints( const int &agendaPoints );

        int baseLink() const;
        void setBaseLink( const int &baseLink );

        QString copyright() const;
        void setCopyright( const QString &copyright );

        QString dataPack() const;
        void setDataPack( const QString &dataPack );

        QString extendedType() const;
        void setExtendedType( const QString &extendedType );

        int extendedTypeNumber() const;
        void setExtendedTypeNumber( const int &extendedTypeNumber );

        QString faction() const;
        void setFaction( const QString &faction );

        int factionNumber() const;
        void setFactionNumber( const int &factionNumber );

        QString flavorText() const;
        void setFlavorText( const QString &flavorText );

        QString guid() const;
        void setGuid( const QString &guid );

        QString illustrator() const;
        void setIllustrator( const QString &illustrator );

        int influenceLimit() const;
        void setInfluenceLimit( const int &influenceLimit );

        int influenceValue() const;
        void setInfluenceValue( const int &influenceValue );

        int installCost() const;
        void setInstallCost( const int &installCost );

        bool isUnique() const;
        void setIsUnique( const bool &isUnique );

        int memoryCost() const;
        void setMemoryCost( const int &memoryCost );

        int minimumDeckSize() const;
        void setMinimumDeckSize( const int &minimumDeckSize );

        int number() const;
        void setNumber( const int &number );

        int playCost() const;
        void setPlayCost( const int &playCost );

        int quantity() const;
        void setQuantity( const int &quantity );

        QString rawText() const;
        void setRawText( const QString &rawText );

        int rezCost() const;
        void setRezCost( const int &rezCost );

        QString side() const;
        void setSide( const QString &side );

        int sideNumber() const;
        void setSideNumber( const int &sideNumber );

        QString set() const;
        void setSet( const QString &set );

        int setNumber() const;
        void setSetNumber( const int &setNumber );

        int strength() const;
        void setStrength( const int &strength );

        QString subtitle() const;
        void setSubtitle( const QString &subtitle );

        QString subtype() const;
        void setSubtype( const QString &subtype );

        QString text() const;
        void setText( const QString &primaryColor, const bool &isDark );

        QString title() const;
        void setTitle( const QString &title );

        int trashCost() const;
        void setTrashCost( const int &trashCost );

        QString type() const;
        void setType( const QString &type );

        int typeNumber() const;
        void setTypeNumber( const int &typeNumber );

signals:
        void dataChanged();

      private:
        int m_advancementRequirement;
        int m_agendaPoints;
        int m_baseLink;
        QString m_copyright;
        QString m_dataPack;
        QString m_extendedType;
        int m_extendedTypeNumber;
        QString m_faction;
        int m_factionNumber;
        QString m_flavorText;
        QString m_guid;
        QString m_illustrator;
        int m_influenceLimit;
        int m_influenceValue;
        int m_installCost;
        bool m_isUnique;
        int m_memoryCost;
        int m_minimumDeckSize;
        int m_number;
        int m_playCost;
        int m_quantity;
        QString m_rawText;
        int m_rezCost;
        QString m_side;
        int m_sideNumber;
        QString m_set;
        int m_setNumber;
        int m_strength;
        QString m_subtitle;
        QString m_subtype;
        QString m_text;
        QString m_title;
        int m_trashCost;
        QString m_type;
        int m_typeNumber;
};

inline int Card::advancementRequirement() const
{
        return m_advancementRequirement;
}

inline int Card::agendaPoints() const
{
        return m_agendaPoints;
}

inline int Card::baseLink() const
{
        return m_baseLink;
}

inline QString Card::copyright() const
{
        return m_copyright;
}

inline QString Card::dataPack() const
{
        return m_dataPack;
}

inline QString Card::extendedType() const
{
        return m_extendedType;
}

inline int Card::extendedTypeNumber() const
{
        return m_extendedTypeNumber;
}

inline QString Card::faction() const
{
        return m_faction;
}

inline int Card::factionNumber() const
{
        return m_factionNumber;
}

inline QString Card::flavorText() const
{
        return m_flavorText;
}

inline QString Card::guid() const
{
        return m_guid;
}

inline QString Card::illustrator() const
{
        return m_illustrator;
}

inline int Card::influenceLimit() const
{
        return m_influenceLimit;
}

inline int Card::influenceValue() const
{
        return m_influenceValue;
}

inline int Card::installCost() const
{
        return m_installCost;
}

inline bool Card::isUnique() const
{
        return m_isUnique;
}

inline int Card::memoryCost() const
{
        return m_memoryCost;
}

inline int Card::minimumDeckSize() const
{
        return m_minimumDeckSize;
}

inline int Card::number() const
{
        return m_number;
}

inline int Card::playCost() const
{
        return m_playCost;
}

inline int Card::quantity() const
{
        return m_quantity;
}

inline QString Card::rawText() const
{
        return m_rawText;
}

inline int Card::rezCost() const
{
        return m_rezCost;
}

inline QString Card::side() const
{
        return m_side;
}

inline int Card::sideNumber() const
{
        return m_sideNumber;
}

inline QString Card::set() const
{
        return m_set;
}

inline int Card::setNumber() const
{
        return m_setNumber;
}

inline int Card::strength() const
{
        return m_strength;
}

inline QString Card::subtitle() const
{
        return m_subtitle;
}

inline QString Card::subtype() const
{
        return m_subtype;
}

inline QString Card::text() const
{
        return m_text;
}

inline QString Card::title() const
{
        return m_title;
}

inline int Card::trashCost() const
{
        return m_trashCost;
}

inline QString Card::type() const
{
        return m_type;
}

inline int Card::typeNumber() const
{
        return m_typeNumber;
}

} // namespace lp

#endif // LP_CARD_HPP
