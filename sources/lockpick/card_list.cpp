// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "lockpick/card_list.hpp"

#include "lockpick/card.hpp"

namespace lp {

CardList::CardList( QObject *parent )
    : QAbstractListModel( parent )
    , m_cards()
{
}

CardList::~CardList()
{
}

void CardList::append( Card *card )
{
        connect( card, &Card::dataChanged, [=] {
                emit dataChanged( index( m_cards.indexOf( card ) ),
                                  index( m_cards.indexOf( card ) ) );
        } );

        beginInsertRows( QModelIndex(), rowCount(), rowCount() );
        m_cards.append( card );
        endInsertRows();
}

Card *CardList::card( const QString &title ) const
{
        for ( Card *card : m_cards ) {
                if ( card->title() == title ) {
                        return card;
                }
        }

        return nullptr;
}

Card *CardList::card( const QString &title, const QString &subtitle ) const
{
        for ( Card *card : m_cards ) {
                if ( ( card->title() == title ) &&
                     ( card->subtitle() == subtitle ) ) {
                        return card;
                }
        }

        return nullptr;
}

Card *CardList::cardFromGuid( const QString &guid ) const
{
        for ( Card *card : m_cards ) {
                if ( card->guid() == guid ) {
                        return card;
                }
        }

        return nullptr;
}

bool CardList::contains( const QString &title ) const
{
        for ( Card *card : m_cards ) {
                if ( card->title() == title ) {
                        return true;
                }
        }

        return false;
}

void CardList::removeOne( lp::Card *card )
{
        int index( m_cards.indexOf( card ) );

        beginRemoveRows( QModelIndex(), index, index );
        m_cards.removeOne( card );
        endRemoveRows();
}

void CardList::reset()
{
        beginRemoveRows( QModelIndex(), 0, m_cards.size() - 1 );
        while ( not m_cards.isEmpty() ) {
                Card *firstCard = m_cards.takeFirst();
                delete firstCard;
                firstCard = nullptr;
        }
        endRemoveRows();
}

QVariant CardList::data( const QModelIndex &index, int role ) const
{
        if ( index.row() < 0 or index.row() >= m_cards.size() ) {
                return QVariant();
        }

        Card *card = m_cards[index.row()];

        switch ( role ) {
        case AdvancementRequirementRole:
                return card->advancementRequirement();
        case AgendaPointsRole:
                return card->agendaPoints();
        case BaseLinkRole:
                return card->baseLink();
        case CopyrightRole:
                return card->copyright();
        case DataPackRole:
                return card->dataPack();
        case ExtendedTypeRole:
                return card->extendedType();
        case ExtendedTypeNumberRole:
                return card->extendedTypeNumber();
        case FactionRole:
                return card->faction();
        case FactionNumberRole:
                return card->factionNumber();
        case FlavorTextRole:
                return card->flavorText();
        case GuidRole:
                return card->guid();
        case IllustratorRole:
                return card->illustrator();
        case InfluenceLimitRole:
                return card->influenceLimit();
        case InfluenceValueRole:
                return card->influenceValue();
        case InstallCostRole:
                return card->installCost();
        case IsUniqueRole:
                return card->isUnique();
        case MemoryCostRole:
                return card->memoryCost();
        case MinimumDeckSizeRole:
                return card->minimumDeckSize();
        case NumberRole:
                return card->number();
        case PlayCostRole:
                return card->playCost();
        case QuantityRole:
                return card->quantity();
        case RezCostRole:
                return card->rezCost();
        case SideRole:
                return card->side();
        case SideNumberRole:
                return card->sideNumber();
        case SetRole:
                return card->set();
        case SetNumberRole:
                return card->setNumber();
        case StrengthRole:
                return card->strength();
        case SubtitleRole:
                return card->subtitle();
        case SubtypeRole:
                return card->subtype();
        case TextRole:
                return card->text();
        case TitleRole:
                return card->title();
        case TrashCostRole:
                return card->trashCost();
        case TypeRole:
                return card->type();
        case TypeNumberRole:
                return card->typeNumber();
        default:
                return QVariant();
        }
}

QHash<int, QByteArray> CardList::roleNames() const
{
        QHash<int, QByteArray> roles;

        roles[AdvancementRequirementRole] = "advancementRequirement";
        roles[AgendaPointsRole] = "agendaPoints";
        roles[BaseLinkRole] = "baseLink";
        roles[CopyrightRole] = "copyright";
        roles[DataPackRole] = "dataPack";
        roles[ExtendedTypeRole] = "extendedType";
        roles[ExtendedTypeNumberRole] = "extendedTypeNumber";
        roles[FactionRole] = "faction";
        roles[FactionNumberRole] = "factionNumber";
        roles[FlavorTextRole] = "flavorText";
        roles[GuidRole] = "guid";
        roles[IllustratorRole] = "illustrator";
        roles[InfluenceLimitRole] = "influenceLimit";
        roles[InfluenceValueRole] = "influenceValue";
        roles[InstallCostRole] = "installCost";
        roles[IsUniqueRole] = "isUnique";
        roles[MemoryCostRole] = "memoryCost";
        roles[MinimumDeckSizeRole] = "minimumDeckSize";
        roles[NumberRole] = "number";
        roles[PlayCostRole] = "playCost";
        roles[QuantityRole] = "quantity";
        roles[RezCostRole] = "rezCost";
        roles[SideRole] = "side";
        roles[SideNumberRole] = "sideNumber";
        roles[SetRole] = "set";
        roles[SetNumberRole] = "setNumber";
        roles[StrengthRole] = "strength";
        roles[SubtitleRole] = "subtitle";
        roles[SubtypeRole] = "subtype";
        roles[TextRole] = "text";
        roles[TitleRole] = "title";
        roles[TrashCostRole] = "trashCost";
        roles[TypeRole] = "type";
        roles[TypeNumberRole] = "typeNumber";

        return roles;
}

int CardList::rowCount( const QModelIndex & ) const
{
        return m_cards.size();
}

bool CardList::setData( const QModelIndex &index, const QVariant &value,
                        int role )
{
        if ( index.row() < 0 or index.row() >= m_cards.size() ) {
                return false;
        }

        Card *card = m_cards[index.row()];

        if ( role == QuantityRole ) {
                card->setQuantity( value.toInt() );
                return true;
        } else {
                return false;
        }
}

} // namespace lp
