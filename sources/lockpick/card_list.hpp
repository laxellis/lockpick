// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef LP_CARD_LIST_HPP
#define LP_CARD_LIST_HPP

#include <QtCore/QAbstractListModel>

namespace lp {

class Card;

class CardList : public QAbstractListModel {
        Q_OBJECT

      public:
        explicit CardList( QObject *parent = 0 );
        virtual ~CardList();

        void append( lp::Card *card );
        Q_INVOKABLE lp::Card *card( const QString &title ) const;
        Q_INVOKABLE lp::Card *card( const QString &title,
                                    const QString &subtitle ) const;
        Card *cardFromGuid( const QString &guid ) const;
        QList<Card *> cards() const;
        bool contains( const QString &title ) const;
        void removeOne( lp::Card *card );
        void reset();

        enum CardRoles {
                AdvancementRequirementRole = Qt::UserRole + 1,
                AgendaPointsRole,
                BaseLinkRole,
                CopyrightRole,
                DataPackRole,
                ExtendedTypeRole,
                ExtendedTypeNumberRole,
                FactionRole,
                FactionNumberRole,
                FlavorTextRole,
                GuidRole,
                IllustratorRole,
                InfluenceLimitRole,
                InfluenceValueRole,
                InstallCostRole,
                IsUniqueRole,
                MemoryCostRole,
                MinimumDeckSizeRole,
                NumberRole,
                PlayCostRole,
                QuantityRole,
                RezCostRole,
                SideRole,
                SideNumberRole,
                SetRole,
                SetNumberRole,
                StrengthRole,
                SubtitleRole,
                SubtypeRole,
                TextRole,
                TitleRole,
                TrashCostRole,
                TypeRole,
                TypeNumberRole
        };

        // Subclassing QAbstractListModel

        QVariant data( const QModelIndex &index,
                       int role = Qt::DisplayRole ) const;
        QHash<int, QByteArray> roleNames() const;
        int rowCount( const QModelIndex &parent = QModelIndex() ) const;
        bool setData( const QModelIndex &index, const QVariant &value,
                      int role = Qt::EditRole );

      private:
        QList<Card *> m_cards;
};

inline QList<Card *> CardList::cards() const
{
        return m_cards;
}

} // namespace lp

#endif // LP_CARD_LIST_HPP
