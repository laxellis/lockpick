// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "lockpick/database.hpp"

#include <QtCore/QFile>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>

#include "lockpick/card.hpp"
#include "lockpick/filter.hpp"
#include "lockpick/sorter.hpp"

namespace lp {

Database::Database( QObject *parent )
    : CardList( parent )
    , m_sorter( new Sorter( this ) )
    , m_filter( new Filter( this ) )
{
        load();
        setupSortFilter();
}

QSortFilterProxyModel *Database::sortedAndFiltered() const
{
        return m_filter->filtered();
}

void Database::updateText( const QString &primaryColor, const bool &isDark )
{
        for ( Card *card : cards() ) {
                card->setText( primaryColor, isDark );
        }
}

void Database::setupSortFilter()
{
        m_sorter->setSourceModel( this );
        m_filter->setSourceModel( m_sorter );
}

void Database::load()
{
        QFile file( QStringLiteral( ":/cards.json" ) );

        file.open( QIODevice::ReadOnly );

        QByteArray data( file.readAll() );

        QJsonDocument jsonDocument( QJsonDocument::fromJson( data ) );

        read( jsonDocument.array() );
}

void Database::read( const QJsonArray &jsonCardArray )
{
        for ( const QJsonValue &jsonValueCard : jsonCardArray ) {
                QJsonObject jsonCard( jsonValueCard.toObject() );

                int advancementRequirement(
                    jsonCard[QStringLiteral( "advancementRequirement" )]
                        .toInt() );
                int agendaPoints(
                    jsonCard[QStringLiteral( "agendaPoints" )].toInt() );
                int baseLink( jsonCard[QStringLiteral( "baseLink" )].toInt() );
                QString copyright(
                    jsonCard[QStringLiteral( "copyright" )].toString() );
                QString dataPack(
                    jsonCard[QStringLiteral( "dataPack" )].toString() );
                QString flavorText(
                    jsonCard[QStringLiteral( "flavorText" )].toString() );
                QString illustrator(
                    jsonCard[QStringLiteral( "illustrator" )].toString() );
                int influenceLimit(
                    jsonCard[QStringLiteral( "influenceLimit" )].toInt() );
                int influenceValue(
                    jsonCard[QStringLiteral( "influenceValue" )].toInt() );
                int installCost(
                    jsonCard[QStringLiteral( "installCost" )].toInt() );
                bool isUnique( jsonCard[QStringLiteral( "unique" )].toBool() );
                int memoryCost(
                    jsonCard[QStringLiteral( "memoryCost" )].toInt() );
                int minimumDeckSize(
                    jsonCard[QStringLiteral( "minimumDeckSize" )].toInt() );
                int number( jsonCard[QStringLiteral( "number" )].toInt() );
                int playCost( jsonCard[QStringLiteral( "playCost" )].toInt() );
                QString rawText(
                    jsonCard[QStringLiteral( "text" )].toString() );
                int rezCost( jsonCard[QStringLiteral( "rezCost" )].toInt() );
                QString side( jsonCard[QStringLiteral( "side" )].toString() );
                int sideNumber( getSideNumber( side ) );
                QString set( jsonCard[QStringLiteral( "set" )].toString() );
                int setNumber( getSetNumber( set ) );
                int strength( jsonCard[QStringLiteral( "strength" )].toInt() );
                QString subtitle(
                    jsonCard[QStringLiteral( "subtitle" )].toString() );
                QString subtype(
                    jsonCard[QStringLiteral( "subtype" )].toString() );
                // QString text(
                //    jsonCard[QStringLiteral( "text" )]
                //        .toString()
                //        .replace( QLatin1String( "<b>" ),
                //                  QLatin1String(
                //                      "<span style=\"color: #448aff\">" ) )
                //        .replace( QLatin1String( "</b>" ),
                //                  QLatin1String( "</span>" ) )
                //        .replace(
                //             QLatin1String( "{{credit}}" ),
                //             QLatin1String(
                //                 "<img src=\"/images/ui/small_credit.svg\" "
                //                 "width=8 height=13 />" ) )
                //        .replace( QLatin1String( "{{click}}" ),
                //                  QLatin1String(
                //                      "<img src=\"/images/ui/small_click.svg\"
                //                      "
                //                      "width=14 height=13 />" ) )
                //        .replace( QLatin1String( "{{recurring credit}}" ),
                //                  QLatin1String( "<img "
                //                                 "src=\"/images/ui/"
                //                                 "small_recurring_credit.svg\"
                //                                 "
                //                                 "width=12 height=13 />" ) )
                //        .replace( QLatin1String( "{{link}}" ),
                //                  QLatin1String(
                //                      "<img src=\"/images/ui/small_link.svg\"
                //                      "
                //                      "width=13 height=13 />" ) )
                //        .replace( QLatin1String( "{{memory unit}}" ),
                //                  QLatin1String( "<img "
                //                                 "src=\"/images/ui/"
                //                                 "small_memory_unit.svg\" "
                //                                 "width=14 height=13 />" ) )
                //        .replace(
                //             QLatin1String( "{{subroutine}}" ),
                //             QLatin1String(
                //                 "<img src=\"/images/ui/small_subroutine.svg\"
                //                 "
                //                 "width=12 height=13 />" ) )
                //        .replace( QLatin1String( "{{trash}}" ),
                //                  QLatin1String(
                //                      "<img src=\"/images/ui/small_trash.svg\"
                //                      "
                //                      "width=11 height=13 />" ) ) );
                QString title( jsonCard[QStringLiteral( "title" )].toString() );
                int trashCost(
                    jsonCard[QStringLiteral( "trashCost" )].toInt() );

                QString faction( getFaction(
                    jsonCard[QStringLiteral( "faction" )].toString(), side ) );
                int factionNumber( getFactionNumber( faction ) );
                QString guid( getGuid( setNumber, number ) );
                QString type( getType(
                    jsonCard[QStringLiteral( "type" )].toString(), side ) );
                int typeNumber( getTypeNumber( type ) );
                QString extendedType( getExtendedType( type, subtype ) );
                int extendedTypeNumber( getExtendedTypeNumber( extendedType ) );

                Card *card = new Card( this );

                card->setAdvancementRequirement( advancementRequirement );
                card->setAgendaPoints( agendaPoints );
                card->setBaseLink( baseLink );
                card->setCopyright( copyright );
                card->setDataPack( dataPack );
                card->setExtendedType( extendedType );
                card->setExtendedTypeNumber( extendedTypeNumber );
                card->setFaction( faction );
                card->setFactionNumber( factionNumber );
                card->setFlavorText( flavorText );
                card->setGuid( guid );
                card->setIllustrator( illustrator );
                card->setInfluenceLimit( influenceLimit );
                card->setInfluenceValue( influenceValue );
                card->setInstallCost( installCost );
                card->setIsUnique( isUnique );
                card->setMemoryCost( memoryCost );
                card->setMinimumDeckSize( minimumDeckSize );
                card->setNumber( number );
                card->setPlayCost( playCost );
                card->setRawText( rawText );
                card->setText( QStringLiteral( "#00bfa5" ), false );
                card->setRezCost( rezCost );
                card->setSide( side );
                card->setSideNumber( sideNumber );
                card->setSet( set );
                card->setSetNumber( setNumber );
                card->setStrength( strength );
                card->setSubtitle( subtitle );
                card->setSubtype( subtype );
                // card->setText( text );
                card->setTitle( title );
                card->setTrashCost( trashCost );
                card->setType( type );
                card->setTypeNumber( typeNumber );

                append( card );
        }
}

QString Database::getExtendedType( const QString &type,
                                   const QString &subtype ) const
{
        if ( type == QLatin1String( "Ice" ) ) {
                if ( subtype.contains( QLatin1String( "Barrier" ) ) ) {
                        return QLatin1String( "Ice: Barrier" );
                } else if ( subtype.contains( QLatin1String( "Code Gate" ) ) ) {
                        return QLatin1String( "Ice: Code Gate" );
                } else if ( subtype.contains( QLatin1String( "Sentry" ) ) ) {
                        return QLatin1String( "Ice: Sentry" );
                } else {
                        return type;
                }
        } else if ( type == QLatin1String( "Program" ) ) {
                if ( subtype.contains( QLatin1String( "Fracter" ) ) ) {
                        return QLatin1String( "Program: Fracter" );
                } else if ( subtype.contains( QLatin1String( "Decoder" ) ) ) {
                        return QLatin1String( "Program: Decoder" );
                } else if ( subtype.contains( QLatin1String( "Killer" ) ) ) {
                        return QLatin1String( "Program: Killer" );
                } else if ( subtype.contains(
                                QLatin1String( "Icebreaker" ) ) ) {
                        return QLatin1String( "Program: Icebreaker" );
                } else {
                        return type;
                }
        } else {
                return type;
        }
}

int Database::getExtendedTypeNumber( const QString &extendedType ) const
{
        if ( extendedType == QLatin1String( "Corporation Identity" ) ) {
                return 0;
        } else if ( extendedType == QLatin1String( "Ice: Barrier" ) ) {
                return 1;
        } else if ( extendedType == QLatin1String( "Ice: Code Gate" ) ) {
                return 2;
        } else if ( extendedType == QLatin1String( "Ice: Sentry" ) ) {
                return 3;
        } else if ( extendedType == QLatin1String( "Ice" ) ) {
                return 4;
        } else if ( extendedType == QLatin1String( "Agenda" ) ) {
                return 5;
        } else if ( extendedType == QLatin1String( "Asset" ) ) {
                return 6;
        } else if ( extendedType == QLatin1String( "Upgrade" ) ) {
                return 7;
        } else if ( extendedType == QLatin1String( "Operation" ) ) {
                return 8;
        } else if ( extendedType == QLatin1String( "Runner Identity" ) ) {
                return 9;
        } else if ( extendedType == QLatin1String( "Program: Fracter" ) ) {
                return 10;
        } else if ( extendedType == QLatin1String( "Program: Decoder" ) ) {
                return 11;
        } else if ( extendedType == QLatin1String( "Program: Killer" ) ) {
                return 12;
        } else if ( extendedType == QLatin1String( "Program: Icebreaker" ) ) {
                return 13;
        } else if ( extendedType == QLatin1String( "Program" ) ) {
                return 14;
        } else if ( extendedType == QLatin1String( "Hardware" ) ) {
                return 15;
        } else if ( extendedType == QLatin1String( "Resource" ) ) {
                return 16;
        } else if ( extendedType == QLatin1String( "Event" ) ) {
                return 17;
        } else {
                return -1;
        }
}

QString Database::getFaction( const QString &faction,
                              const QString &side ) const
{
        if ( faction == QLatin1String( "Neutral" ) ) {
                if ( side == QLatin1String( "Corporation" ) ) {
                        return QLatin1String( "Neutral Corporation" );
                } else {
                        return QLatin1String( "Neutral Runner" );
                }
        } else {
                return faction;
        }
}

int Database::getFactionNumber( const QString &faction ) const
{
        if ( faction == QLatin1String( "Haas-Bioroid" ) ) {
                return 0;
        } else if ( faction == QLatin1String( "Jinteki" ) ) {
                return 1;
        } else if ( faction == QLatin1String( "NBN" ) ) {
                return 2;
        } else if ( faction == QLatin1String( "Weyland Consortium" ) ) {
                return 3;
        } else if ( faction == QLatin1String( "Neutral Corporation" ) ) {
                return 4;
        } else if ( faction == QLatin1String( "Anarch" ) ) {
                return 5;
        } else if ( faction == QLatin1String( "Criminal" ) ) {
                return 6;
        } else if ( faction == QLatin1String( "Shaper" ) ) {
                return 7;
        } else if ( faction == QLatin1String( "Neutral Runner" ) ) {
                return 8;
        } else {
                return -1;
        }
}

QString Database::getGuid( const int &setNumber, const int &number ) const
{
        QString cardGuidHead(
            QLatin1String( "bc0f047c-01b1-427f-a439-d451eda" ) );
        QString rightJustifiedSetNumber(
            QString::number( setNumber ).rightJustified( 2, '0' ) );
        QString rightJustifiedNumber(
            QString::number( number ).rightJustified( 3, '0' ) );
        QString cardGuiTail( rightJustifiedSetNumber + rightJustifiedNumber );

        return cardGuidHead + cardGuiTail;
}

int Database::getSetNumber( const QString &set ) const
{
        if ( set == QLatin1String( "Core Set" ) ) {
                return 1;
        } else if ( set == QLatin1String( "Genesis" ) ) {
                return 2;
        } else if ( set == QLatin1String( "Creation and Control" ) ) {
                return 3;
        } else if ( set == QLatin1String( "Spin" ) ) {
                return 4;
        } else if ( set == QLatin1String( "Honor and Profit" ) ) {
                return 5;
        } else if ( set == QLatin1String( "Lunar" ) ) {
                return 6;
        } else if ( set == QLatin1String( "Order and Chaos" ) ) {
                return 7;
        } else if ( set == QLatin1String( "SanSan" ) ) {
                return 8;
        } else {
                return -1;
        }
}
int Database::getSideNumber( const QString &side ) const
{
        if ( side == QLatin1String( "Corporation" ) ) {
                return 0;
        } else if ( side == QLatin1String( "Runner" ) ) {
                return 1;
        } else {
                return -1;
        }
}

QString Database::getType( const QString &type, const QString &side ) const
{
        if ( type == QLatin1String( "Identity" ) ) {
                if ( side == QLatin1String( "Corporation" ) ) {
                        return QLatin1String( "Corporation Identity" );
                } else {
                        return QLatin1String( "Runner Identity" );
                }
        } else {
                return type;
        }
}

int Database::getTypeNumber( const QString &type ) const
{
        if ( type == QLatin1String( "Corporation Identity" ) ) {
                return 0;
        } else if ( type == QLatin1String( "Ice" ) ) {
                return 1;
        } else if ( type == QLatin1String( "Agenda" ) ) {
                return 2;
        } else if ( type == QLatin1String( "Asset" ) ) {
                return 3;
        } else if ( type == QLatin1String( "Upgrade" ) ) {
                return 4;
        } else if ( type == QLatin1String( "Operation" ) ) {
                return 5;
        } else if ( type == QLatin1String( "Runner Identity" ) ) {
                return 6;
        } else if ( type == QLatin1String( "Program" ) ) {
                return 7;
        } else if ( type == QLatin1String( "Hardware" ) ) {
                return 8;
        } else if ( type == QLatin1String( "Resource" ) ) {
                return 9;
        } else if ( type == QLatin1String( "Event" ) ) {
                return 10;
        } else {
                return -1;
        }
}

} // namespace lp
