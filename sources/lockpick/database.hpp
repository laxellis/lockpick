// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef LP_DATABASE_HPP
#define LP_DATABASE_HPP

#include "lockpick/card_list.hpp"

class QSortFilterProxyModel;

namespace lp {

class Sorter;
class Filter;

class Database : public CardList {
        Q_OBJECT

        Q_PROPERTY( lp::Sorter *sorter READ sorter CONSTANT )
        Q_PROPERTY( lp::Filter *filter READ filter CONSTANT )
        Q_PROPERTY( QSortFilterProxyModel *sortedAndFiltered READ
                        sortedAndFiltered CONSTANT )

      public:
        explicit Database( QObject *parent = 0 );

        Sorter *sorter() const;
        Filter *filter() const;
        QSortFilterProxyModel *sortedAndFiltered() const;

      public slots:
        void updateText( const QString &primaryColor, const bool &isDark );

      private:
        Sorter *m_sorter;
        Filter *m_filter;

        void setupSortFilter();

        // Load cards data
        void load();
        void read( const QJsonArray &jsonCardArray );

        // Parse data
        QString getExtendedType( const QString &type,
                                 const QString &subtype ) const;
        int getExtendedTypeNumber( const QString &extendedType ) const;
        QString getFaction( const QString &faction, const QString &side ) const;
        int getFactionNumber( const QString &faction ) const;
        QString getGuid( const int &setNumber, const int &number ) const;
        int getSetNumber( const QString &set ) const;
        int getSideNumber( const QString &side ) const;
        QString getType( const QString &type, const QString &side ) const;
        int getTypeNumber( const QString &type ) const;
};

inline Sorter *Database::sorter() const
{
        return m_sorter;
}

inline Filter *Database::filter() const
{
        return m_filter;
}

} // namespace lp

#endif // LP_DATABASE_HPP
