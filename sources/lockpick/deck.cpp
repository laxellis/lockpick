// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "lockpick/deck.hpp"

#include <math.h>

#include <QtCore/QFile>
#include <QtCore/QJsonArray>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QXmlStreamWriter>

#include "lockpick/card.hpp"
#include "lockpick/database.hpp"
#include "lockpick/sorter.hpp"

namespace lp {

Deck::Deck( QObject *parent )
    : CardList( parent )
    , m_name()
    , m_version( QLatin1String( "1.0" ) )
    , m_side()
    , m_faction()
    , m_title()
    , m_subtitle()
    , m_agendaRestriction( 20 )
    , m_agendaPoints( 0 )
    , m_minimumDeckSize( 45 )
    , m_deckSize( 0 )
    , m_influenceLimit( 15 )
    , m_influenceValue( 0 )
    , m_identityNumber( -1 )
    , m_identitySet()
    , m_identity( nullptr )
    , m_sorter( new Sorter( this ) )
    , m_deckUrl()
    , m_database( nullptr )
{
        setupConnections();
        setupSorter();
}

void Deck::add( const int &quantity, lp::Card *newCard )
{
        if ( newCard->type().contains( QLatin1String( "Identity" ) ) ) {
                setIdentity( newCard );
                return;
        }

        if ( m_identity ) {
                if ( newCard->side() != m_side ) {
                        return;
                }
                if ( ( newCard->type() == QLatin1String( "Agenda" ) ) &&
                     ( ( newCard->faction() != m_faction ) &&
                       newCard->faction() !=
                           QLatin1String( "Neutral Corporation" ) ) ) {
                        return;
                }
        }

        if ( contains( newCard->title() ) ) {
                Card *existingCard = card( newCard->title() );
                int existingCardQuantity( existingCard->quantity() );
                existingCard->setQuantity( existingCardQuantity + quantity );
        } else {
                Card *deckCard = new Card( newCard, this );
                deckCard->setQuantity( quantity );
                append( deckCard );
        }
        setDeckSize();
        setInfluenceValue();
        setAgendaPoints();
}

void Deck::exportAsOctgnDeck( const QUrl &url )
{
        if ( url not_eq m_deckUrl ) {
                m_deckUrl = url;
        }

        QString path( url.toLocalFile() );

        toXml( url );
}

// void Deck::importOctgnDeck( const QUrl &url )
//{
//        newDeck();
//
//        if ( url not_eq m_deckUrl ) {
//                m_deckUrl = url;
//        }
//
//        fromXml( url );
//}

void Deck::newDeck()
{
        reset();
        deleteIdentity();
        setName( QStringLiteral( "" ) );
        setVersion( QStringLiteral( "1.0" ) );
        setSide( QStringLiteral( "" ) );
        setFaction( QStringLiteral( "" ) );
        setTitle( QStringLiteral( "" ) );
        setSubtitle( QStringLiteral( "" ) );
        setMinimumDeckSize( 45 );
        setInfluenceLimit( 15 );
        setIdentityNumber( -1 );
        setIdentitySet( QStringLiteral( "" ) );
        setDeckSize();
        setAgendaPoints();
        setAgendaRestriction();
        setInfluenceValue();
        m_deckUrl.clear();
}

void Deck::open( const QUrl &url )
{
        newDeck();

        if ( url not_eq m_deckUrl ) {
                m_deckUrl = url;
        }

        QString path( url.toLocalFile() );

        QFile file( path );

        file.open( QIODevice::ReadOnly );

        if ( path.endsWith( QLatin1String( ".lpd" ) ) ) {
                QByteArray data( file.readAll() );

                QJsonDocument jsonDocument( QJsonDocument::fromJson( data ) );

                fromJson( jsonDocument.object() );
        } else if ( path.endsWith( QLatin1String( ".o8d" ) ) ) {
                QXmlStreamReader stream( &file );

                fromXml( stream );
        }
}

void Deck::remove( const int &quantity, lp::Card *existingCard )
{
        if ( not existingCard ) {
                return;
        }

        if ( quantity < existingCard->quantity() ) {
                int quantityOfExistingCard( existingCard->quantity() );
                existingCard->setQuantity( quantityOfExistingCard - quantity );
        } else {
                removeOne( existingCard );
        }

        setDeckSize();
        setInfluenceValue();
        setAgendaPoints();
}

void Deck::save()
{
        if ( m_deckUrl.isEmpty() ) {
                emit openSaveAsFileDialog();
        } else {
                saveAs( m_deckUrl );
        }
}

void Deck::saveAs( const QUrl &url )
{
        if ( url not_eq m_deckUrl ) {
                m_deckUrl = url;
        }

        QString path( url.toLocalFile() );

        if ( not path.endsWith( QLatin1String( ".lpd" ) ) ) {
                path.append( QLatin1String( ".lpd" ) );
        }

        QJsonDocument jsonDocument;
        jsonDocument.setObject( toJson() );

        QByteArray data( jsonDocument.toJson() );

        QFile file( path );
        file.open( QIODevice::WriteOnly );
        file.write( data );
}

void Deck::setQuantity( const int &quantity, lp::Card *existingCard )
{
        if ( existingCard->type().contains( QLatin1String( "Identity" ) ) ) {
                setIdentity( existingCard );
                return;
        }

        if ( m_identity ) {
                if ( existingCard->side() != m_side ) {
                        return;
                }
                if ( ( existingCard->type() == QLatin1String( "Agenda" ) ) &&
                     ( ( existingCard->faction() != m_faction ) &&
                       existingCard->faction() !=
                           QLatin1String( "Neutral Corporation" ) ) ) {
                        return;
                }
        }

        if ( contains( existingCard->title() ) ) {
                Card *deckCard = card( existingCard->title() );
                deckCard->setQuantity( quantity );
        } else {
                Card *deckCard = new Card( existingCard, this );
                deckCard->setQuantity( quantity );
                append( deckCard );
        }

        setDeckSize();
        setInfluenceValue();
        setAgendaPoints();
}

void Deck::setName( const QString &name )
{
        if ( name not_eq m_name ) {
                m_name = name;
                emit nameChanged();
        }
}

void Deck::setVersion( const QString &version )
{
        if ( version not_eq m_version ) {
                m_version = version;
                emit versionChanged();
        }
}

QSortFilterProxyModel *Deck::sorted() const
{
        return m_sorter;
}

void Deck::setDatabase( Database *database )
{
        if ( database not_eq m_database ) {
                m_database = database;
        }
}

void Deck::fromJson( const QJsonObject &jsonObject )
{
        QString name( jsonObject[QStringLiteral( "name" )].toString() );
        QString version( jsonObject[QStringLiteral( "version" )].toString() );
        QJsonObject jsonIdentity(
            jsonObject[QStringLiteral( "identity" )].toObject() );
        QJsonArray jsonCards( jsonObject[QStringLiteral( "cards" )].toArray() );

        QString identityTitle(
            jsonIdentity[QStringLiteral( "title" )].toString() );
        QString identitySubtitle(
            jsonIdentity[QStringLiteral( "subtitle" )].toString() );

        setName( name );
        setVersion( version );

        add( 1, m_database->card( identityTitle, identitySubtitle ) );

        for ( const QJsonValue &jsonValueCard : jsonCards ) {
                QJsonObject jsonCard( jsonValueCard.toObject() );

                QString title( jsonCard[QStringLiteral( "title" )].toString() );
                int quantity( jsonCard[QStringLiteral( "quantity" )].toInt() );

                add( quantity, m_database->card( title ) );
        }
}

void Deck::setupConnections() const
{
        QObject::connect( this, &lp::Deck::deckSizeChanged, this,
                          &lp::Deck::setAgendaRestriction );
}

void Deck::setupSorter()
{
        m_sorter->setSourceModel( this );
        m_sorter->sortBy( "Type" );
}

void Deck::setIdentity( Card *identity )
{
        if ( !m_identity ) {
                createIdentity( identity );
        }
        if ( ( identity->title() not_eq m_identity->title() ) ||
             ( identity->subtitle() not_eq m_identity->subtitle() ) ) {
                deleteIdentity();
                createIdentity( identity );
        }
}

void Deck::createIdentity( Card *identity )
{
        m_identity = new Card( identity, this );

        setSide( m_identity->side() );
        setFaction( m_identity->faction() );
        setTitle( m_identity->title() );
        setSubtitle( m_identity->subtitle() );
        setMinimumDeckSize( m_identity->minimumDeckSize() );
        setInfluenceLimit( m_identity->influenceLimit() );
        setIdentityNumber( m_identity->number() );
        setIdentitySet( m_identity->set() );

        for ( Card *card : cards() ) {
                // Remove cards that do not belong to this side.
                if ( card->side() != m_side ) {
                        removeOne( card );
                }
                // Remove agendas that do not belong to this faction.
                if ( ( card->type() == QLatin1String( "Agenda" ) ) &&
                     ( ( card->faction() != m_faction ) &&
                       card->faction() !=
                           QLatin1String( "Neutral Corporation" ) ) ) {
                        removeOne( card );
                }
        }

        setInfluenceValue();
        setAgendaRestriction();
}

void Deck::deleteIdentity()
{
        delete m_identity;
        m_identity = nullptr;
}

void Deck::setSide( const QString &side )
{
        if ( side not_eq m_side ) {
                m_side = side;
                emit sideChanged();
        }
}

void Deck::setFaction( const QString &faction )
{
        if ( faction not_eq m_faction ) {
                m_faction = faction;
                emit factionChanged();
        }
}

void Deck::setTitle( const QString &title )
{
        if ( title not_eq m_title ) {
                m_title = title;
                emit titleChanged();
        }
}

void Deck::setSubtitle( const QString &subtitle )
{
        if ( subtitle not_eq m_subtitle ) {
                m_subtitle = subtitle;
                emit subtitleChanged();
        }
}

void Deck::setAgendaRestriction()
{
        int agendaRestriction( 0 );
        int minimumAgendaRestriction( 0 );

        agendaRestriction = floor( m_deckSize / 5 ) * 2 + 2;
        minimumAgendaRestriction = floor( m_minimumDeckSize / 5 ) * 2 + 2;

        if ( agendaRestriction < minimumAgendaRestriction ) {
                agendaRestriction = minimumAgendaRestriction;
        }

        if ( agendaRestriction not_eq m_agendaRestriction ) {
                m_agendaRestriction = agendaRestriction;
                emit agendaRestrictionChanged();
        }
}

void Deck::setAgendaPoints()
{
        int agendaPoints( 0 );

        for ( Card *card : cards() ) {
                if ( card->type() == QLatin1String( "Agenda" ) ) {
                        agendaPoints += card->agendaPoints() * card->quantity();
                }
        }

        if ( agendaPoints not_eq m_agendaPoints ) {
                m_agendaPoints = agendaPoints;
                emit agendaPointsChanged();
        }
}

void Deck::setMinimumDeckSize( const int &minimumDeckSize )
{
        if ( minimumDeckSize not_eq m_minimumDeckSize ) {
                m_minimumDeckSize = minimumDeckSize;
                emit minimumDeckSizeChanged();
        }
}

void Deck::setDeckSize()
{
        int deckSize( 0 );

        for ( Card *card : cards() ) {
                deckSize += card->quantity();
        }

        if ( deckSize not_eq m_deckSize ) {
                m_deckSize = deckSize;
                emit deckSizeChanged();
        }
}

void Deck::setInfluenceLimit( const int &influenceLimit )
{
        if ( influenceLimit not_eq m_influenceLimit ) {
                m_influenceLimit = influenceLimit;
                emit influenceLimitChanged();
        }
}

void Deck::setInfluenceValue()
{
        int influenceValue( 0 );

        for ( Card *card : cards() ) {
                if ( card->faction() not_eq m_faction ) {
                        influenceValue +=
                            card->influenceValue() * card->quantity();
                }
        }

        if ( influenceValue not_eq m_influenceValue ) {
                m_influenceValue = influenceValue;
                emit influenceValueChanged();
        }
}

void Deck::setIdentityNumber( const int &number )
{
        if ( number not_eq m_identityNumber ) {
                m_identityNumber = number;
                emit identityNumberChanged();
        }
}

void Deck::setIdentitySet( const QString &set )
{
        if ( set not_eq m_identitySet ) {
                m_identitySet = set;
                emit identitySetChanged();
        }
}

QJsonObject Deck::toJson() const
{
        QJsonObject jsonIdentity;

        jsonIdentity[QStringLiteral( "title" )] = m_title;
        jsonIdentity[QStringLiteral( "subtitle" )] = m_subtitle;

        QJsonArray jsonCards;

        for ( Card *card : cards() ) {
                QJsonObject jsonCard;

                jsonCard[QStringLiteral( "title" )] = card->title();
                jsonCard[QStringLiteral( "quantity" )] = card->quantity();

                jsonCards.append( jsonCard );
        }

        QJsonObject jsonObject;

        jsonObject[QStringLiteral( "name" )] = m_name;
        jsonObject[QStringLiteral( "version" )] = m_version;
        jsonObject[QStringLiteral( "identity" )] = jsonIdentity;
        jsonObject[QStringLiteral( "cards" )] = jsonCards;

        return jsonObject;
}

void Deck::fromXml( QXmlStreamReader &stream )
{
        // QString path( url.toLocalFile() );

        // QFile file( path );

        // file.open( QIODevice::ReadOnly );

        // QXmlStreamReader stream( file );

        while ( not stream.atEnd() ) {
                stream.readNextStartElement();
                if ( ( stream.name() == "card" ) &&
                     ( stream.isStartElement() ) ) {
                        // qDebug() << stream.name();
                        // qDebug() << stream.attributes().value( "qty" );
                        int qty = stream.attributes().value( "qty" ).toInt();
                        // qDebug() << stream.attributes().value( "id" );
                        QString id =
                            stream.attributes().value( "id" ).toString();
                        add( qty, m_database->cardFromGuid( id ) );
                }
        }
}

void Deck::toXml( const QUrl &url ) const
{
        QString path( url.toLocalFile() );

        if ( not path.endsWith( QLatin1String( ".o8d" ) ) ) {
                path.append( QLatin1String( ".o8d" ) );
        }

        QFile file( path );
        file.open( QIODevice::WriteOnly );

        QXmlStreamWriter stream( &file );
        stream.setAutoFormatting( true );
        stream.writeStartDocument( QStringLiteral( "1.0" ), true );
        stream.writeStartElement( QStringLiteral( "deck" ) );
        stream.writeAttribute(
            QStringLiteral( "game" ),
            QStringLiteral( "0f38e453-26df-4c04-9d67-6d43de939c77" ) );

        stream.writeStartElement( QStringLiteral( "section" ) );
        stream.writeAttribute( QStringLiteral( "name" ),
                               QStringLiteral( "Identity" ) );
        stream.writeStartElement( QStringLiteral( "card" ) );
        stream.writeAttribute( QStringLiteral( "qty" ), QStringLiteral( "1" ) );
        stream.writeAttribute( QStringLiteral( "id" ), m_identity->guid() );
        stream.writeCharacters( m_title + ": " + m_subtitle );
        stream.writeEndElement(); // card
        stream.writeEndElement(); // Identity section

        stream.writeStartElement( QStringLiteral( "section" ) );
        stream.writeAttribute( QStringLiteral( "name" ),
                               QStringLiteral( "R&D / Stack" ) );
        for ( Card *card : cards() ) {
                stream.writeStartElement( QStringLiteral( "card" ) );
                stream.writeAttribute( QStringLiteral( "qty" ),
                                       QString::number( card->quantity() ) );
                stream.writeAttribute( QStringLiteral( "id" ), card->guid() );
                stream.writeCharacters( card->title() );
                stream.writeEndElement(); // card
        }
        stream.writeEndElement(); // R&D / Stack section

        stream.writeEndElement(); // deck
        stream.writeEndDocument();
}

} // namespace lp
