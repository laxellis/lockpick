// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef LP_DECK_HPP
#define LP_DECK_HPP

#include "lockpick/card_list.hpp"

#include <QtCore/QXmlStreamReader>
#include <QtCore/QUrl>

class QSortFilterProxyModel;

namespace lp {

class Database;
class Sorter;

class Deck : public CardList {
        Q_OBJECT

        Q_PROPERTY( QString name READ name WRITE setName NOTIFY nameChanged )
        Q_PROPERTY( QString version READ version WRITE setVersion NOTIFY
                        versionChanged )
        Q_PROPERTY( QString side READ side NOTIFY sideChanged )
        Q_PROPERTY( QString faction READ faction NOTIFY factionChanged )
        Q_PROPERTY( QString title READ title NOTIFY titleChanged )
        Q_PROPERTY( QString subtitle READ subtitle NOTIFY subtitleChanged )
        Q_PROPERTY( int agendaRestriction READ agendaRestriction NOTIFY
                        agendaRestrictionChanged )
        Q_PROPERTY( int agendaPoints READ agendaPoints NOTIFY
                        agendaPointsChanged )
        Q_PROPERTY( int minimumDeckSize READ minimumDeckSize NOTIFY
                        minimumDeckSizeChanged )
        Q_PROPERTY( int deckSize READ deckSize NOTIFY deckSizeChanged )
        Q_PROPERTY( int influenceLimit READ influenceLimit NOTIFY
                        influenceLimitChanged )
        Q_PROPERTY( int influenceValue READ influenceValue NOTIFY
                        influenceValueChanged )
        Q_PROPERTY( int identityNumber READ identityNumber NOTIFY
                        identityNumberChanged )
        Q_PROPERTY( QString identitySet READ identitySet NOTIFY
                        identitySetChanged )
        Q_PROPERTY( lp::Sorter *sorter READ sorter CONSTANT )
        Q_PROPERTY( QSortFilterProxyModel *sorted READ sorted CONSTANT )

      public:
        explicit Deck( QObject *parent = 0 );

        Q_INVOKABLE void add( const int &quantity, lp::Card *newCard );
        Q_INVOKABLE void exportAsOctgnDeck( const QUrl &url );
        // Q_INVOKABLE void importOctgnDeck( const QUrl &url );
        Q_INVOKABLE void newDeck();
        Q_INVOKABLE void open( const QUrl &url );
        Q_INVOKABLE void remove( const int &quantity, lp::Card *existingCard );
        Q_INVOKABLE void save();
        Q_INVOKABLE void saveAs( const QUrl &url );
        Q_INVOKABLE void setQuantity( const int &quantity,
                                      lp::Card *existingCard );

        QString name() const;
        void setName( const QString &name );
        QString version() const;
        void setVersion( const QString &version );
        QString side() const;
        QString faction() const;
        QString title() const;
        QString subtitle() const;
        int agendaRestriction() const;
        int agendaPoints() const;
        int minimumDeckSize() const;
        int deckSize() const;
        int influenceLimit() const;
        int influenceValue() const;
        int identityNumber() const;
        QString identitySet() const;
        Sorter *sorter() const;
        QSortFilterProxyModel *sorted() const;
        void setDatabase( Database *database );

signals:
        void nameChanged();
        void versionChanged();
        void sideChanged();
        void factionChanged();
        void titleChanged();
        void subtitleChanged();
        void agendaRestrictionChanged();
        void agendaPointsChanged();
        void minimumDeckSizeChanged();
        void deckSizeChanged();
        void influenceLimitChanged();
        void influenceValueChanged();
        void identityNumberChanged();
        void identitySetChanged();
        void openSaveAsFileDialog();
        void openExportAsOctgnDeckFileDialog();

      private:
        QString m_name;
        QString m_version;
        QString m_side;
        QString m_faction;
        QString m_title;
        QString m_subtitle;
        int m_agendaRestriction;
        int m_agendaPoints;
        int m_minimumDeckSize;
        int m_deckSize;
        int m_influenceLimit;
        int m_influenceValue;
        int m_identityNumber;
        QString m_identitySet;
        Card *m_identity;
        Sorter *m_sorter;
        QUrl m_deckUrl;
        Database *m_database;

        void fromJson( const QJsonObject &jsonObject );
        void setupConnections() const;
        void setupSorter();
        void setIdentity( Card *identity );
        void createIdentity( Card *identity );
        void deleteIdentity();
        void setSide( const QString &side );
        void setFaction( const QString &faction );
        void setTitle( const QString &title );
        void setSubtitle( const QString &subtitle );
        void setAgendaRestriction();
        void setAgendaPoints();
        void setMinimumDeckSize( const int &minimumDeckSize );
        void setDeckSize();
        void setInfluenceLimit( const int &influenceLimit );
        void setInfluenceValue();
        void setIdentityNumber( const int &number );
        void setIdentitySet( const QString &set );
        QJsonObject toJson() const;
        void fromXml( QXmlStreamReader &stream );
        void toXml( const QUrl &url ) const;
};

inline QString Deck::name() const
{
        return m_name;
}

inline QString Deck::version() const
{
        return m_version;
}

inline QString Deck::side() const
{
        return m_side;
}

inline QString Deck::faction() const
{
        return m_faction;
}

inline QString Deck::title() const
{
        return m_title;
}

inline QString Deck::subtitle() const
{
        return m_subtitle;
}

inline int Deck::agendaRestriction() const
{
        return m_agendaRestriction;
}

inline int Deck::agendaPoints() const
{
        return m_agendaPoints;
}

inline int Deck::minimumDeckSize() const
{
        return m_minimumDeckSize;
}

inline int Deck::deckSize() const
{
        return m_deckSize;
}

inline int Deck::influenceLimit() const
{
        return m_influenceLimit;
}

inline int Deck::influenceValue() const
{
        return m_influenceValue;
}

inline int Deck::identityNumber() const
{
        return m_identityNumber;
}

inline QString Deck::identitySet() const
{
        return m_identitySet;
}

inline Sorter *Deck::sorter() const
{
        return m_sorter;
}

} // namespace lp

#endif // LP_DECK_HPP
