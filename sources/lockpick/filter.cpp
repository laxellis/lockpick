// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "lockpick/filter.hpp"

#include <QtCore/QSortFilterProxyModel>
#include <QtCore/QStateMachine>
#include <QtCore/QTimer>

#include "lockpick/card_list.hpp"
#include "lockpick/toggle_state.hpp"

namespace lp {

Filter::Filter( QObject *parent )
    : QObject( parent )
    , m_stateMachine( new QStateMachine( this ) )
    , m_initial( new QState( QState::ParallelStates, m_stateMachine ) )
    , m_side( new QSortFilterProxyModel( this ) )
    , m_sideList()
    , m_runner( new ToggleState( m_initial ) )
    , m_corporation( new ToggleState( m_initial ) )
    , m_faction( new QSortFilterProxyModel( this ) )
    , m_factionList()
    , m_anarch( new ToggleState( m_initial ) )
    , m_criminal( new ToggleState( m_initial ) )
    , m_shaper( new ToggleState( m_initial ) )
    , m_neutralRunner( new ToggleState( m_initial ) )
    , m_haasBioroid( new ToggleState( m_initial ) )
    , m_jinteki( new ToggleState( m_initial ) )
    , m_nbn( new ToggleState( m_initial ) )
    , m_weylandConsortium( new ToggleState( m_initial ) )
    , m_neutralCorporation( new ToggleState( m_initial ) )
    , m_type( new QSortFilterProxyModel( this ) )
    , m_typeList()
    , m_runnerIdentity( new ToggleState( m_initial ) )
    , m_program( new ToggleState( m_initial ) )
    , m_hardware( new ToggleState( m_initial ) )
    , m_resource( new ToggleState( m_initial ) )
    , m_eventCard( new ToggleState( m_initial ) )
    , m_corporationIdentity( new ToggleState( m_initial ) )
    , m_agenda( new ToggleState( m_initial ) )
    , m_ice( new ToggleState( m_initial ) )
    , m_asset( new ToggleState( m_initial ) )
    , m_upgrade( new ToggleState( m_initial ) )
    , m_operation( new ToggleState( m_initial ) )
    , m_set( new QSortFilterProxyModel( this ) )
    , m_setList()
    , m_coreSet( new ToggleState( m_initial ) )
    , m_genesis( new ToggleState( m_initial ) )
    , m_creationAndControl( new ToggleState( m_initial ) )
    , m_spin( new ToggleState( m_initial ) )
    , m_honorAndProfit( new ToggleState( m_initial ) )
    , m_lunar( new ToggleState( m_initial ) )
    , m_orderAndChaos( new ToggleState( m_initial ) )
    , m_search( new QSortFilterProxyModel( this ) )
    , m_searchRole()
{
        searchBy( QStringLiteral( "Title" ) );
        setupLists();
        setupFilters();
        setupStateMachine();
        QTimer::singleShot( 0, m_stateMachine, SLOT( start() ) );
}

void Filter::search( const QString &text )
{
        m_search->setFilterRegExp( text );
}

void Filter::searchBy( const QString &searchRole )
{
        if ( searchRole == QLatin1String( "Subtype" ) ) {
                m_search->setFilterRole( CardList::SubtypeRole );
        } else if ( searchRole == QLatin1String( "Text" ) ) {
                m_search->setFilterRole( CardList::TextRole );
        } else if ( searchRole == QLatin1String( "Title" ) ) {
                m_search->setFilterRole( CardList::TitleRole );
        }

        if ( searchRole not_eq m_searchRole ) {
                m_searchRole = searchRole;
                emit searchRoleChanged();
        }
}

void Filter::setSourceModel( QAbstractItemModel *sourceModel )
{
        m_side->setSourceModel( sourceModel );
}

void Filter::toggleRunner()
{
        m_runner->toggle();
        if ( m_runner->isOff() ) {
                m_sideList.removeOne( QLatin1String( "Runner" ) );
        } else {
                m_sideList.append( QLatin1String( "Runner" ) );
        }
        filterSide();
}

void Filter::toggleCorporation()
{
        m_corporation->toggle();
        if ( m_corporation->isOff() ) {
                m_sideList.removeOne( QLatin1String( "Corporation" ) );
        } else {
                m_sideList.append( QLatin1String( "Corporation" ) );
        }
        filterSide();
}

void Filter::toggleAnarch()
{
        m_anarch->toggle();
        if ( m_anarch->isOff() ) {
                m_factionList.removeOne( QLatin1String( "Anarch" ) );
        } else {
                m_factionList.append( QLatin1String( "Anarch" ) );
        }
        filterFaction();
}

void Filter::toggleCriminal()
{
        m_criminal->toggle();
        if ( m_criminal->isOff() ) {
                m_factionList.removeOne( QLatin1String( "Criminal" ) );
        } else {
                m_factionList.append( QLatin1String( "Criminal" ) );
        }
        filterFaction();
}

void Filter::toggleShaper()
{
        m_shaper->toggle();
        if ( m_shaper->isOff() ) {
                m_factionList.removeOne( QLatin1String( "Shaper" ) );
        } else {
                m_factionList.append( QLatin1String( "Shaper" ) );
        }
        filterFaction();
}

void Filter::toggleNeutralRunner()
{
        m_neutralRunner->toggle();
        if ( m_neutralRunner->isOff() ) {
                m_factionList.removeOne( QLatin1String( "Neutral Runner" ) );
        } else {
                m_factionList.append( QLatin1String( "Neutral Runner" ) );
        }
        filterFaction();
}

void Filter::toggleHaasBioroid()
{
        m_haasBioroid->toggle();
        if ( m_haasBioroid->isOff() ) {
                m_factionList.removeOne( QLatin1String( "Haas-Bioroid" ) );
        } else {
                m_factionList.append( QLatin1String( "Haas-Bioroid" ) );
        }
        filterFaction();
}

void Filter::toggleJinteki()
{
        m_jinteki->toggle();
        if ( m_jinteki->isOff() ) {
                m_factionList.removeOne( QLatin1String( "Jinteki" ) );
        } else {
                m_factionList.append( QLatin1String( "Jinteki" ) );
        }
        filterFaction();
}

void Filter::toggleNbn()
{
        m_nbn->toggle();
        if ( m_nbn->isOff() ) {
                m_factionList.removeOne( QLatin1String( "NBN" ) );
        } else {
                m_factionList.append( QLatin1String( "NBN" ) );
        }
        filterFaction();
}

void Filter::toggleWeylandConsortium()
{
        m_weylandConsortium->toggle();
        if ( m_weylandConsortium->isOff() ) {
                m_factionList.removeOne(
                    QLatin1String( "Weyland Consortium" ) );
        } else {
                m_factionList.append( QLatin1String( "Weyland Consortium" ) );
        }
        filterFaction();
}

void Filter::toggleNeutralCorporation()
{
        m_neutralCorporation->toggle();
        if ( m_neutralCorporation->isOff() ) {
                m_factionList.removeOne(
                    QLatin1String( "Neutral Corporation" ) );
        } else {
                m_factionList.append( QLatin1String( "Neutral Corporation" ) );
        }
        filterFaction();
}

void Filter::toggleRunnerIdentity()
{
        m_runnerIdentity->toggle();
        if ( m_runnerIdentity->isOff() ) {
                m_typeList.removeOne( QLatin1String( "Runner Identity" ) );
        } else {
                m_typeList.append( QLatin1String( "Runner Identity" ) );
        }
        filterType();
}

void Filter::toggleProgram()
{
        m_program->toggle();
        if ( m_program->isOff() ) {
                m_typeList.removeOne( QLatin1String( "Program" ) );
        } else {
                m_typeList.append( QLatin1String( "Program" ) );
        }
        filterType();
}

void Filter::toggleHardware()
{
        m_hardware->toggle();
        if ( m_hardware->isOff() ) {
                m_typeList.removeOne( QLatin1String( "Hardware" ) );
        } else {
                m_typeList.append( QLatin1String( "Hardware" ) );
        }
        filterType();
}

void Filter::toggleResource()
{
        m_resource->toggle();
        if ( m_resource->isOff() ) {
                m_typeList.removeOne( QLatin1String( "Resource" ) );
        } else {
                m_typeList.append( QLatin1String( "Resource" ) );
        }
        filterType();
}

void Filter::toggleEvent()
{
        m_eventCard->toggle();
        if ( m_eventCard->isOff() ) {
                m_typeList.removeOne( QLatin1String( "Event" ) );
        } else {
                m_typeList.append( QLatin1String( "Event" ) );
        }
        filterType();
}

void Filter::toggleCorporationIdentity()
{
        m_corporationIdentity->toggle();
        if ( m_corporationIdentity->isOff() ) {
                m_typeList.removeOne( QLatin1String( "Corporation Identity" ) );
        } else {
                m_typeList.append( QLatin1String( "Corporation Identity" ) );
        }
        filterType();
}

void Filter::toggleAgenda()
{
        m_agenda->toggle();
        if ( m_agenda->isOff() ) {
                m_typeList.removeOne( QLatin1String( "Agenda" ) );
        } else {
                m_typeList.append( QLatin1String( "Agenda" ) );
        }
        filterType();
}

void Filter::toggleIce()
{
        m_ice->toggle();
        if ( m_ice->isOff() ) {
                m_typeList.removeOne( QLatin1String( "Ice" ) );
        } else {
                m_typeList.append( QLatin1String( "Ice" ) );
        }
        filterType();
}

void Filter::toggleAsset()
{
        m_asset->toggle();
        if ( m_asset->isOff() ) {
                m_typeList.removeOne( QLatin1String( "Asset" ) );
        } else {
                m_typeList.append( QLatin1String( "Asset" ) );
        }
        filterType();
}

void Filter::toggleUpgrade()
{
        m_upgrade->toggle();
        if ( m_upgrade->isOff() ) {
                m_typeList.removeOne( QLatin1String( "Upgrade" ) );
        } else {
                m_typeList.append( QLatin1String( "Upgrade" ) );
        }
        filterType();
}

void Filter::toggleOperation()
{
        m_operation->toggle();
        if ( m_operation->isOff() ) {
                m_typeList.removeOne( QLatin1String( "Operation" ) );
        } else {
                m_typeList.append( QLatin1String( "Operation" ) );
        }
        filterType();
}

void Filter::toggleCoreSet()
{
        m_coreSet->toggle();
        if ( m_coreSet->isOff() ) {
                m_setList.removeOne( QLatin1String( "Core Set" ) );
        } else {
                m_setList.append( QLatin1String( "Core Set" ) );
        }
        filterSet();
}

void Filter::toggleGenesis()
{
        m_genesis->toggle();
        if ( m_genesis->isOff() ) {
                m_setList.removeOne( QLatin1String( "Genesis" ) );
        } else {
                m_setList.append( QLatin1String( "Genesis" ) );
        }
        filterSet();
}

void Filter::toggleCreationAndControl()
{
        m_creationAndControl->toggle();
        if ( m_creationAndControl->isOff() ) {
                m_setList.removeOne( QLatin1String( "Creation and Control" ) );
        } else {
                m_setList.append( QLatin1String( "Creation and Control" ) );
        }
        filterSet();
}

void Filter::toggleSpin()
{
        m_spin->toggle();
        if ( m_spin->isOff() ) {
                m_setList.removeOne( QLatin1String( "Spin" ) );
        } else {
                m_setList.append( QLatin1String( "Spin" ) );
        }
        filterSet();
}

void Filter::toggleHonorAndProfit()
{
        m_honorAndProfit->toggle();
        if ( m_honorAndProfit->isOff() ) {
                m_setList.removeOne( QLatin1String( "Honor and Profit" ) );
        } else {
                m_setList.append( QLatin1String( "Honor and Profit" ) );
        }
        filterSet();
}

void Filter::toggleLunar()
{
        m_lunar->toggle();
        if ( m_lunar->isOff() ) {
                m_setList.removeOne( QLatin1String( "Lunar" ) );
        } else {
                m_setList.append( QLatin1String( "Lunar" ) );
        }
        filterSet();
}

void Filter::toggleOrderAndChaos()
{
        m_orderAndChaos->toggle();
        if ( m_orderAndChaos->isOff() ) {
                m_setList.removeOne( QLatin1String( "Order and Chaos" ) );
        } else {
                m_setList.append( QLatin1String( "Order and Chaos" ) );
        }
        filterSet();
}

void Filter::filterFaction()
{
        m_faction->setFilterRegExp(
            m_factionList.join( QStringLiteral( "|" ) ) );
}

void Filter::filterSet()
{
        m_set->setFilterRegExp( m_setList.join( QStringLiteral( "|" ) ) );
}

void Filter::filterSide()
{
        m_side->setFilterRegExp( m_sideList.join( QStringLiteral( "|" ) ) );
}

void Filter::filterType()
{
        m_type->setFilterRegExp( m_typeList.join( QStringLiteral( "|" ) ) );
}

void Filter::setupFilters()
{
        m_side->setFilterRole( CardList::SideRole );
        m_faction->setSourceModel( m_side );
        m_faction->setFilterRole( CardList::FactionRole );
        m_type->setSourceModel( m_faction );
        m_type->setFilterRole( CardList::TypeRole );
        m_set->setSourceModel( m_type );
        m_set->setFilterRole( CardList::SetRole );
        m_search->setSourceModel( m_set );
        // m_search->setFilterRole( CardList::TitleRole );
        m_search->setFilterCaseSensitivity( Qt::CaseInsensitive );
}

void Filter::setupLists()
{
        m_sideList << "Runner"
                   << "Corporation"
                   << "None";
        m_factionList << "Anarch"
                      << "Criminal"
                      << "Shaper"
                      << "Neutral Runner"
                      << "Haas-Bioroid"
                      << "Jinteki"
                      << "NBN"
                      << "Weyland Consortium"
                      << "Neutral Corporation"
                      << "None";
        m_typeList << "Runner Identity"
                   << "Program"
                   << "Hardware"
                   << "Resource"
                   << "Event"
                   << "Corporation Identity"
                   << "Agenda"
                   << "Ice"
                   << "Asset"
                   << "Upgrade"
                   << "Operation"
                   << "None";
        m_setList << "Core Set"
                  << "Genesis"
                  << "Creation and Control"
                  << "Spin"
                  << "Honor and Profit"
                  << "Lunar"
                  << "Order and Chaos"
                  << "None";
}

void Filter::setupStateMachine()
{
        m_stateMachine->setInitialState( m_initial );
}

} // namespace lp
