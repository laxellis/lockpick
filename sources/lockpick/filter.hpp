// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef LP_FILTER_HPP
#define LP_FILTER_HPP

#include <QtCore/QObject>
#include <QtCore/QStringList>

class QAbstractItemModel;
class QSortFilterProxyModel;
class QState;
class QStateMachine;

namespace lp {

class ToggleState;

class Filter : public QObject {
        Q_OBJECT

        Q_PROPERTY( lp::ToggleState *runner READ runner CONSTANT )
        Q_PROPERTY( lp::ToggleState *corporation READ corporation CONSTANT )
        Q_PROPERTY( lp::ToggleState *anarch READ anarch CONSTANT )
        Q_PROPERTY( lp::ToggleState *criminal READ criminal CONSTANT )
        Q_PROPERTY( lp::ToggleState *shaper READ shaper CONSTANT )
        Q_PROPERTY( lp::ToggleState *neutralRunner READ neutralRunner CONSTANT )
        Q_PROPERTY( lp::ToggleState *haasBioroid READ haasBioroid CONSTANT )
        Q_PROPERTY( lp::ToggleState *jinteki READ jinteki CONSTANT )
        Q_PROPERTY( lp::ToggleState *nbn READ nbn CONSTANT )
        Q_PROPERTY( lp::ToggleState *weylandConsortium READ weylandConsortium
                        CONSTANT )
        Q_PROPERTY( lp::ToggleState *neutralCorporation READ neutralCorporation
                        CONSTANT )
        Q_PROPERTY( lp::ToggleState *runnerIdentity READ runnerIdentity
                        CONSTANT )
        Q_PROPERTY( lp::ToggleState *program READ program CONSTANT )
        Q_PROPERTY( lp::ToggleState *hardware READ hardware CONSTANT )
        Q_PROPERTY( lp::ToggleState *resource READ resource CONSTANT )
        Q_PROPERTY( lp::ToggleState *eventCard READ eventCard CONSTANT )
        Q_PROPERTY( lp::ToggleState *corporationIdentity READ
                        corporationIdentity CONSTANT )
        Q_PROPERTY( lp::ToggleState *agenda READ agenda CONSTANT )
        Q_PROPERTY( lp::ToggleState *ice READ ice CONSTANT )
        Q_PROPERTY( lp::ToggleState *asset READ asset CONSTANT )
        Q_PROPERTY( lp::ToggleState *upgrade READ upgrade CONSTANT )
        Q_PROPERTY( lp::ToggleState *operation READ operation CONSTANT )
        Q_PROPERTY( lp::ToggleState *coreSet READ coreSet CONSTANT )
        Q_PROPERTY( lp::ToggleState *genesis READ genesis CONSTANT )
        Q_PROPERTY( lp::ToggleState *creationAndControl READ creationAndControl
                        CONSTANT )
        Q_PROPERTY( lp::ToggleState *spin READ spin CONSTANT )
        Q_PROPERTY( lp::ToggleState *honorAndProfit READ honorAndProfit
                        CONSTANT )
        Q_PROPERTY( lp::ToggleState *lunar READ lunar CONSTANT )
        Q_PROPERTY( lp::ToggleState *orderAndChaos READ orderAndChaos CONSTANT )
        Q_PROPERTY( QSortFilterProxyModel *filtered READ filtered CONSTANT )
        Q_PROPERTY( QString searchRole READ searchRole NOTIFY
                        searchRoleChanged )

      public:
        explicit Filter( QObject *parent = 0 );

        Q_INVOKABLE void search( const QString &text );
        Q_INVOKABLE void searchBy( const QString &searchRole );
        void setSourceModel( QAbstractItemModel *sourceModel );
        Q_INVOKABLE void toggleRunner();
        Q_INVOKABLE void toggleCorporation();
        Q_INVOKABLE void toggleAnarch();
        Q_INVOKABLE void toggleCriminal();
        Q_INVOKABLE void toggleShaper();
        Q_INVOKABLE void toggleNeutralRunner();
        Q_INVOKABLE void toggleHaasBioroid();
        Q_INVOKABLE void toggleJinteki();
        Q_INVOKABLE void toggleNbn();
        Q_INVOKABLE void toggleWeylandConsortium();
        Q_INVOKABLE void toggleNeutralCorporation();
        Q_INVOKABLE void toggleRunnerIdentity();
        Q_INVOKABLE void toggleProgram();
        Q_INVOKABLE void toggleHardware();
        Q_INVOKABLE void toggleResource();
        Q_INVOKABLE void toggleEvent();
        Q_INVOKABLE void toggleCorporationIdentity();
        Q_INVOKABLE void toggleAgenda();
        Q_INVOKABLE void toggleIce();
        Q_INVOKABLE void toggleAsset();
        Q_INVOKABLE void toggleUpgrade();
        Q_INVOKABLE void toggleOperation();
        Q_INVOKABLE void toggleCoreSet();
        Q_INVOKABLE void toggleGenesis();
        Q_INVOKABLE void toggleCreationAndControl();
        Q_INVOKABLE void toggleSpin();
        Q_INVOKABLE void toggleHonorAndProfit();
        Q_INVOKABLE void toggleLunar();
        Q_INVOKABLE void toggleOrderAndChaos();

        ToggleState *runner() const;
        ToggleState *corporation() const;
        ToggleState *anarch() const;
        ToggleState *criminal() const;
        ToggleState *shaper() const;
        ToggleState *neutralRunner() const;
        ToggleState *haasBioroid() const;
        ToggleState *jinteki() const;
        ToggleState *nbn() const;
        ToggleState *weylandConsortium() const;
        ToggleState *neutralCorporation() const;
        ToggleState *runnerIdentity() const;
        ToggleState *program() const;
        ToggleState *hardware() const;
        ToggleState *resource() const;
        ToggleState *eventCard() const;
        ToggleState *corporationIdentity() const;
        ToggleState *agenda() const;
        ToggleState *ice() const;
        ToggleState *asset() const;
        ToggleState *upgrade() const;
        ToggleState *operation() const;
        ToggleState *coreSet() const;
        ToggleState *genesis() const;
        ToggleState *creationAndControl() const;
        ToggleState *spin() const;
        ToggleState *honorAndProfit() const;
        ToggleState *lunar() const;
        ToggleState *orderAndChaos() const;
        QSortFilterProxyModel *filtered() const;
        QString searchRole() const;

signals:
        void searchRoleChanged();

      private:
        QStateMachine *m_stateMachine;
        QState *m_initial;
        QSortFilterProxyModel *m_side;
        QStringList m_sideList;
        ToggleState *m_runner;
        ToggleState *m_corporation;
        QSortFilterProxyModel *m_faction;
        QStringList m_factionList;
        ToggleState *m_anarch;
        ToggleState *m_criminal;
        ToggleState *m_shaper;
        ToggleState *m_neutralRunner;
        ToggleState *m_haasBioroid;
        ToggleState *m_jinteki;
        ToggleState *m_nbn;
        ToggleState *m_weylandConsortium;
        ToggleState *m_neutralCorporation;
        QSortFilterProxyModel *m_type;
        QStringList m_typeList;
        ToggleState *m_runnerIdentity;
        ToggleState *m_program;
        ToggleState *m_hardware;
        ToggleState *m_resource;
        ToggleState *m_eventCard;
        ToggleState *m_corporationIdentity;
        ToggleState *m_agenda;
        ToggleState *m_ice;
        ToggleState *m_asset;
        ToggleState *m_upgrade;
        ToggleState *m_operation;
        QSortFilterProxyModel *m_set;
        QStringList m_setList;
        ToggleState *m_coreSet;
        ToggleState *m_genesis;
        ToggleState *m_creationAndControl;
        ToggleState *m_spin;
        ToggleState *m_honorAndProfit;
        ToggleState *m_lunar;
        ToggleState *m_orderAndChaos;
        QSortFilterProxyModel *m_search;
        QString m_searchRole;

        void filterFaction();
        void filterSet();
        void filterSide();
        void filterType();
        void setupFilters();
        void setupLists();
        void setupStateMachine();
};

inline ToggleState *Filter::runner() const
{
        return m_runner;
}

inline ToggleState *Filter::corporation() const
{
        return m_corporation;
}

inline ToggleState *Filter::anarch() const
{
        return m_anarch;
}

inline ToggleState *Filter::criminal() const
{
        return m_criminal;
}

inline ToggleState *Filter::shaper() const
{
        return m_shaper;
}

inline ToggleState *Filter::neutralRunner() const
{
        return m_neutralRunner;
}

inline ToggleState *Filter::haasBioroid() const
{
        return m_haasBioroid;
}

inline ToggleState *Filter::jinteki() const
{
        return m_jinteki;
}

inline ToggleState *Filter::nbn() const
{
        return m_nbn;
}

inline ToggleState *Filter::weylandConsortium() const
{
        return m_weylandConsortium;
}

inline ToggleState *Filter::neutralCorporation() const
{
        return m_neutralCorporation;
}

inline ToggleState *Filter::runnerIdentity() const
{
        return m_runnerIdentity;
}

inline ToggleState *Filter::program() const
{
        return m_program;
}

inline ToggleState *Filter::hardware() const
{
        return m_hardware;
}

inline ToggleState *Filter::resource() const
{
        return m_resource;
}

inline ToggleState *Filter::eventCard() const
{
        return m_eventCard;
}

inline ToggleState *Filter::corporationIdentity() const
{
        return m_corporationIdentity;
}

inline ToggleState *Filter::agenda() const
{
        return m_agenda;
}

inline ToggleState *Filter::ice() const
{
        return m_ice;
}

inline ToggleState *Filter::asset() const
{
        return m_asset;
}

inline ToggleState *Filter::upgrade() const
{
        return m_upgrade;
}

inline ToggleState *Filter::operation() const
{
        return m_operation;
}

inline ToggleState *Filter::coreSet() const
{
        return m_coreSet;
}

inline ToggleState *Filter::genesis() const
{
        return m_genesis;
}

inline ToggleState *Filter::creationAndControl() const
{
        return m_creationAndControl;
}

inline ToggleState *Filter::spin() const
{
        return m_spin;
}

inline ToggleState *Filter::honorAndProfit() const
{
        return m_honorAndProfit;
}

inline ToggleState *Filter::lunar() const
{
        return m_lunar;
}

inline ToggleState *Filter::orderAndChaos() const
{
        return m_orderAndChaos;
}

inline QSortFilterProxyModel *Filter::filtered() const
{
        return m_search;
}

inline QString Filter::searchRole() const
{
        return m_searchRole;
}

} // namespace lp

#endif // LP_FILTER_HPP
