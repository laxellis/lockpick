// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "lockpick/lockpick_controller.hpp"

#include <QtGui/QScreen>
#include <QtQml/QtQml>
#include <QtQuick/QQuickView>

#include "lockpick/card.hpp"
#include "lockpick/database.hpp"
#include "lockpick/deck.hpp"
#include "lockpick/filter.hpp"
#include "lockpick/sorter.hpp"
#include "lockpick/state.hpp"
#include "lockpick/state_machine.hpp"
#include "lockpick/toggle_state.hpp"
#include "lockpick/utility.hpp"
#include "lockpick/ui/color.hpp"
#include "lockpick/ui/font.hpp"
#include "lockpick/ui/image.hpp"
#include "lockpick/ui/ui.hpp"

namespace lp {

LockpickController::LockpickController( QObject *parent )
    : QObject( parent )
    , m_database( new Database( this ) )
    , m_deck( new Deck( this ) )
    , m_stateMachine( new StateMachine( this ) )
    , m_ui( new ui::Ui( this ) )
    , m_utility( new Utility( this ) )
    , m_window( new QQuickView() )
    , m_engine( m_window->engine() )
{
        setupConnections();
        setupQml();
        setupWindow();
        m_deck->setDatabase( m_database );
        // m_ui->loadTheme( QUrl( "qrc:/themes/light.lpt" ) );
}

void LockpickController::setupConnections() const
{
        connect( m_ui, &ui::Ui::styleChanged, m_database,
                 &Database::updateText );
}

void LockpickController::setupQml() const
{
        // Register types
        qmlRegisterType<QSortFilterProxyModel>();
        qmlRegisterType<lp::Card>();
        qmlRegisterType<lp::State>();
        qmlRegisterType<lp::ToggleState>();
        qmlRegisterType<lp::ui::Color>();
        qmlRegisterType<lp::ui::Font>();
        qmlRegisterType<lp::ui::Image>();

        // Set context properties
        m_engine->rootContext()->setContextProperty(
            QStringLiteral( "lpDatabase" ), m_database );
        m_engine->rootContext()->setContextProperty( QStringLiteral( "lpDeck" ),
                                                     m_deck );
        m_engine->rootContext()->setContextProperty(
            QStringLiteral( "deckSorter" ), m_deck->sorter() );
        m_engine->rootContext()->setContextProperty( QStringLiteral( "filter" ),
                                                     m_database->filter() );
        m_engine->rootContext()->setContextProperty( QStringLiteral( "sorter" ),
                                                     m_database->sorter() );
        m_engine->rootContext()->setContextProperty(
            QStringLiteral( "stateMachine" ), m_stateMachine );
        m_engine->rootContext()->setContextProperty( QStringLiteral( "ui" ),
                                                     m_ui );
        m_engine->rootContext()->setContextProperty(
            QStringLiteral( "utility" ), m_utility );
        // m_engine->rootContext()->setContextProperty( QStringLiteral( "lp" ),
        // this );
}

void LockpickController::setupWindow()
{
        m_window->setSource(
            QUrl( QStringLiteral( "qrc:/qml/LockpickContent.qml" ) ) );
        m_window->setResizeMode( QQuickView::SizeRootObjectToView );

        QScreen *screen( m_window->screen() );
        QRect screenRect( screen->availableGeometry() );

        int width( screenRect.width() * 0.8 );
        int height( screenRect.height() * 0.8 );
        int x( ( screenRect.width() - width ) / 2 + screenRect.x() );
        int y( ( screenRect.height() - height ) / 2 + screenRect.y() );

        m_window->setX( x );
        m_window->setY( y );
        m_window->setWidth( width );
        m_window->setHeight( height );

        // m_window->setColor( m_ui->color()->card() );

        m_window->show();
}

} // namespace lp
