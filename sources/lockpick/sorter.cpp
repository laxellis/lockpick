// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "lockpick/sorter.hpp"

#include "lockpick/card_list.hpp"

namespace lp {

Sorter::Sorter( QObject *parent )
    : QSortFilterProxyModel( parent )
    , m_role()
{
        sortBy( QStringLiteral( "Faction" ) );
}

void Sorter::sortBy( const QString &role )
{
        if ( role not_eq m_role ) {
                m_role = role;
                sort( -1 );
                sort( 0 );
                emit roleChanged();
        }
}

bool Sorter::lessThan( const QModelIndex &left, const QModelIndex &right ) const
{
        QString leftString;
        QString rightString;

        QVariant leftExtendedTypeNumber(
            sourceModel()->data( left, CardList::ExtendedTypeNumberRole ) );
        QVariant rightExtendedTypeNumber(
            sourceModel()->data( right, CardList::ExtendedTypeNumberRole ) );
        QVariant leftFactionNumber(
            sourceModel()->data( left, CardList::FactionNumberRole ) );
        QVariant rightFactionNumber(
            sourceModel()->data( right, CardList::FactionNumberRole ) );
        QVariant leftNumber(
            sourceModel()->data( left, CardList::NumberRole ) );
        QVariant rightNumber(
            sourceModel()->data( right, CardList::NumberRole ) );
        QVariant leftSetNumber(
            sourceModel()->data( left, CardList::SetNumberRole ) );
        QVariant rightSetNumber(
            sourceModel()->data( right, CardList::SetNumberRole ) );
        QVariant leftTitle( sourceModel()->data( left, CardList::TitleRole ) );
        QVariant rightTitle(
            sourceModel()->data( right, CardList::TitleRole ) );

        if ( m_role == QLatin1String( "Deck" ) ) {
                leftString.append(
                               leftExtendedTypeNumber.toString().rightJustified(
                                   2, '0' ) ).append( leftTitle.toString() );
                rightString.append( rightExtendedTypeNumber.toString()
                                        .rightJustified( 2, '0' ) )
                    .append( rightTitle.toString() );
        }
        if ( m_role == QLatin1String( "Faction" ) ) {
                leftString.append( leftFactionNumber.toString() )
                    .append( leftExtendedTypeNumber.toString().rightJustified(
                        2, '0' ) )
                    .append( leftTitle.toString() );
                rightString.append( rightFactionNumber.toString() )
                    .append( rightExtendedTypeNumber.toString().rightJustified(
                        2, '0' ) )
                    .append( rightTitle.toString() );

        } else if ( m_role == QLatin1String( "Set" ) ) {
                leftString.append( leftSetNumber.toString() )
                    .append( leftNumber.toString().rightJustified( 3, '0' ) );
                rightString.append( rightSetNumber.toString() )
                    .append( rightNumber.toString().rightJustified( 3, '0' ) );
        } else if ( m_role == QLatin1String( "Title" ) ) {
                leftString.append( leftTitle.toString() );
                rightString.append( rightTitle.toString() );
        } else if ( m_role == QLatin1String( "Type" ) ) {
                leftString.append(
                               leftExtendedTypeNumber.toString().rightJustified(
                                   2, '0' ) )
                    .append( leftFactionNumber.toString() )
                    .append( leftTitle.toString() );
                rightString.append( rightExtendedTypeNumber.toString()
                                        .rightJustified( 2, '0' ) )
                    .append( rightFactionNumber.toString() )
                    .append( rightTitle.toString() );
        }

        return QString::compare( leftString, rightString,
                                 Qt::CaseInsensitive ) < 0;
}

} // namespace lp
