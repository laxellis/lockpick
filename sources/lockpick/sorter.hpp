// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef LP_SORTER_HPP
#define LP_SORTER_HPP

#include <QtCore/QSortFilterProxyModel>

namespace lp {

class Sorter : public QSortFilterProxyModel {
        Q_OBJECT

        Q_PROPERTY( QString role READ role NOTIFY roleChanged )

      public:
        explicit Sorter( QObject *parent = 0 );

        Q_INVOKABLE void sortBy( const QString &role );

        QString role() const;

signals:
        void roleChanged();

      private:
        QString m_role;

        bool lessThan( const QModelIndex &left,
                       const QModelIndex &right ) const;
};

inline QString Sorter::role() const
{
        return m_role;
}

} // namespace lp

#endif // LP_SORTER_HPP
