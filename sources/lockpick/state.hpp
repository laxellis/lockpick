// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef LP_STATE_HPP
#define LP_STATE_HPP

#include <QtCore/QState>

namespace lp {

class State : public QState {
        Q_OBJECT

        Q_PROPERTY( bool isActive READ isActive NOTIFY isActiveChanged )

      public:
        explicit State( QState *parent = 0 );

        bool isActive() const;

signals:
        void isActiveChanged();

      protected:
        void onEntry( QEvent * );
        void onExit( QEvent * );

      private:
        bool m_isActive;
};

inline bool State::isActive() const
{
        return m_isActive;
}

} // namespace lp

#endif // LP_STATE_HPP
