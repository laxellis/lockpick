// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "lockpick/state_machine.hpp"

#include <QtCore/QTimer>

#include "lockpick/state.hpp"
#include "lockpick/toggle_state.hpp"

namespace lp {

StateMachine::StateMachine( QObject *parent )
    : QStateMachine( parent )
    , m_initial( new QState( QState::ParallelStates, this ) )
    , m_filtersPanel( new ToggleState( m_initial ) )
    , m_aboutDialog( new QState( m_initial ) )
    , m_hiddenAboutDialog( new State( m_aboutDialog ) )
    , m_visibleAboutDialog( new State( m_aboutDialog ) )
{
        setupStateMachine();
        setupTransitions();
        QTimer::singleShot( 0, this, SLOT( start() ) );
}

void StateMachine::toggleFiltersPanel()
{
        m_filtersPanel->toggle();
}

void StateMachine::setupStateMachine()
{
        m_aboutDialog->setInitialState( m_hiddenAboutDialog );
        setInitialState( m_initial );
}

void StateMachine::setupTransitions()
{
        m_hiddenAboutDialog->addTransition( this, SIGNAL( showAboutDialog() ),
                                            m_visibleAboutDialog );
        m_visibleAboutDialog->addTransition( this, SIGNAL( hideAboutDialog() ),
                                             m_hiddenAboutDialog );
}

} // namespace lp
