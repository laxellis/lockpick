// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef LP_STATE_MACHINE_HPP
#define LP_STATE_MACHINE_HPP

#include <QtCore/QStateMachine>

namespace lp {

class State;
class ToggleState;

class StateMachine : public QStateMachine {
        Q_OBJECT

        Q_PROPERTY( lp::ToggleState *filtersPanel READ filtersPanel CONSTANT )
        Q_PROPERTY( QState *aboutDialog READ aboutDialog CONSTANT )
        Q_PROPERTY( lp::State *hiddenAboutDialog READ hiddenAboutDialog
                        CONSTANT )
        Q_PROPERTY( lp::State *visibleAboutDialog READ visibleAboutDialog
                        CONSTANT )

      public:
        explicit StateMachine( QObject *parent = 0 );

        Q_INVOKABLE void toggleFiltersPanel();

        ToggleState *filtersPanel() const;
        QState *aboutDialog() const;
        State *hiddenAboutDialog() const;
        State *visibleAboutDialog() const;

signals:
        void hideAboutDialog();
        void showAboutDialog();

      private:
        QState *m_initial;
        ToggleState *m_filtersPanel;
        QState *m_aboutDialog;
        State *m_hiddenAboutDialog;
        State *m_visibleAboutDialog;

        void setupStateMachine();
        void setupTransitions();
};

inline ToggleState *StateMachine::filtersPanel() const
{
        return m_filtersPanel;
}

inline QState *StateMachine::aboutDialog() const
{
        return m_aboutDialog;
}

inline State *StateMachine::hiddenAboutDialog() const
{
        return m_hiddenAboutDialog;
}

inline State *StateMachine::visibleAboutDialog() const
{
        return m_visibleAboutDialog;
}

} // namespace lp

#endif // LP_STATE_MACHINE_HPP
