// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "lockpick/toggle_state.hpp"

#include <QtCore/QVariant>

namespace lp {

ToggleState::ToggleState( QState *parent )
    : QState( parent )
    , m_on( new QState( this ) )
    , m_off( new QState( this ) )
    , m_isOn( true )
{
        setupConnections();
        setupState();
}

void ToggleState::setIsOn( const bool &isOn )
{
        if ( isOn not_eq m_isOn ) {
                m_isOn = isOn;
                emit isOnChanged();
        }
}

void ToggleState::setupConnections() const
{
        connect( this, &ToggleState::isOnChanged, this,
                 &ToggleState::isOffChanged );
}

void ToggleState::setupState()
{
        setInitialState( m_on );

        m_on->assignProperty( this, "isOn", QVariant( true ) );
        m_off->assignProperty( this, "isOn", QVariant( false ) );

        m_on->addTransition( this, SIGNAL( toggle() ), m_off );
        m_off->addTransition( this, SIGNAL( toggle() ), m_on );
}

} // namespace lp
