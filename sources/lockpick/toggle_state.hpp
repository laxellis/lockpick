// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef LP_TOGGLE_STATE_HPP
#define LP_TOGGLE_STATE_HPP

#include <QtCore/QState>

namespace lp {

class ToggleState : public QState {
        Q_OBJECT

        Q_PROPERTY( bool isOn READ isOn WRITE setIsOn NOTIFY isOnChanged )
        Q_PROPERTY( bool isOff READ isOff NOTIFY isOffChanged )

      public:
        explicit ToggleState( QState *parent = 0 );

      public:
        bool isOn() const;
        void setIsOn( const bool &isOn );
        bool isOff() const;

signals:

        void toggle();
        void isOnChanged();
        void isOffChanged();

      private:
        QState *m_on;
        QState *m_off;
        bool m_isOn;

        void setupConnections() const;
        void setupState();
};

inline bool ToggleState::isOn() const
{
        return m_isOn;
}

inline bool ToggleState::isOff() const
{
        return not isOn();
}

} // namespace lp

#endif // LP_TOGGLE_STATE_HPP
