// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "lockpick/ui/color.hpp"

#include <QtCore/QJsonObject>

namespace lp {
namespace ui {

Color::Color( QObject *parent )
    : QObject( parent )
    , m_app( QLatin1String( "#e0e0e0" ) )
    , m_appBar( QLatin1String( "#424242" ) )
    , m_card( QLatin1String( "#fafafa" ) )
    , m_cardHighlight( QLatin1String( "#eeeeee" ) )
    , m_toolBar( QLatin1String( "#424242" ) )
    , m_accent( QLatin1String( "#00c853" ) )
    , m_ripple( QLatin1String( "#ff5252" ) )
    , m_primary( QLatin1String( "#00bfa5" ) )
    , m_content( QLatin1String( "#000000" ) )
    , m_divider( QLatin1String( "#1e000000" ) )
    , m_disabled( QLatin1String( "#42000000" ) )
    , m_hintText( QLatin1String( "#42000000" ) )
    , m_secondaryText( QLatin1String( "#89000000" ) )
    , m_text( QLatin1String( "#dd000000" ) )
    , m_icon( QLatin1String( "89000000" ) )
    , m_corporation( QLatin1String( "#3f51b5" ) )
    , m_runner( QLatin1String( "#f44336" ) )
    , m_haasBioroid( QLatin1String( "#673ab7" ) )
    , m_jinteki( QLatin1String( "#f44336" ) )
    , m_nbn( QLatin1String( "#fbc02d" ) )
    , m_weylandConsortium( QLatin1String( "#009688" ) )
    , m_anarch( QLatin1String( "#ff9800" ) )
    , m_criminal( QLatin1String( "#2196f3" ) )
    , m_shaper( QLatin1String( "#8bc34a" ) )
    , m_neutral( QLatin1String( "#9e9e9e" ) )
{
}

QColor Color::faction( const QString &faction ) const
{
        if ( faction == QLatin1String( "Haas-Bioroid" ) ) {
                return m_haasBioroid;
        } else if ( faction == QLatin1String( "Jinteki" ) ) {
                return m_jinteki;
        } else if ( faction == QLatin1String( "NBN" ) ) {
                return m_nbn;
        } else if ( faction == QLatin1String( "Weyland Consortium" ) ) {
                return m_weylandConsortium;
        } else if ( faction == QLatin1String( "Anarch" ) ) {
                return m_anarch;
        } else if ( faction == QLatin1String( "Criminal" ) ) {
                return m_criminal;
        } else if ( faction == QLatin1String( "Shaper" ) ) {
                return m_shaper;
        } else {
                return m_neutral;
        }
}

QColor Color::dividerFrom( const QColor &background )
{
        QColor divider;

        if ( isDark( background ) ) {
                divider.setNamedColor( QLatin1String( "white" ) );
                divider.setAlphaF( 0.12 );
        } else {
                divider.setNamedColor( QLatin1String( "black" ) );
                divider.setAlphaF( 0.12 );
        }

        return divider;
}

QColor Color::disabledFrom( const QColor &background )
{
        QColor disabled;

        if ( isDark( background ) ) {
                disabled.setNamedColor( QLatin1String( "white" ) );
                disabled.setAlphaF( 0.30 );
        } else {
                disabled.setNamedColor( QLatin1String( "black" ) );
                disabled.setAlphaF( 0.26 );
        }

        return disabled;
}

QColor Color::hintTextFrom( const QColor &background )
{
        QColor hintText;

        if ( isDark( background ) ) {
                hintText.setNamedColor( QLatin1String( "white" ) );
                hintText.setAlphaF( 0.30 );
        } else {
                hintText.setNamedColor( QLatin1String( "black" ) );
                hintText.setAlphaF( 0.26 );
        }

        return hintText;
}

QColor Color::secondaryTextFrom( const QColor &background )
{
        QColor secondaryText;

        if ( isDark( background ) ) {
                secondaryText.setNamedColor( QLatin1String( "white" ) );
                secondaryText.setAlphaF( 0.70 );
        } else {
                secondaryText.setNamedColor( QLatin1String( "black" ) );
                secondaryText.setAlphaF( 0.54 );
        }

        return secondaryText;
}

QColor Color::textFrom( const QColor &background )
{
        QColor text;

        if ( isDark( background ) ) {
                text.setNamedColor( QLatin1String( "white" ) );
                text.setAlphaF( 1.0 );
        } else {
                text.setNamedColor( QLatin1String( "black" ) );
                text.setAlphaF( 0.87 );
        }

        return text;
}

QColor Color::iconFrom( const QColor &background )
{
        QColor icon;

        if ( isDark( background ) ) {
                icon.setNamedColor( QLatin1String( "white" ) );
                icon.setAlphaF( 1.0 );
        } else {
                icon.setNamedColor( QLatin1String( "black" ) );
                icon.setAlphaF( 0.54 );
        }

        return icon;
}

void Color::fromJson( const QJsonValue &jsonValue )
{
        QJsonObject jsonObject( jsonValue.toObject() );

        QJsonValue app( jsonObject.value( QStringLiteral( "app" ) ) );
        QJsonValue appBar( jsonObject.value( QStringLiteral( "appBar" ) ) );
        QJsonValue toolBar( jsonObject.value( QStringLiteral( "toolBar" ) ) );
        QJsonValue primary( jsonObject.value( QStringLiteral( "primary" ) ) );
        QJsonValue card( jsonObject.value( QStringLiteral( "card" ) ) );
        QJsonValue cardHighlight(
            jsonObject.value( QStringLiteral( "cardHighlight" ) ) );

        setApp( QColor( app.toString( QStringLiteral( "#e0e0e0" ) ) ) );
        setAppBar( QColor( appBar.toString( QStringLiteral( "#424242" ) ) ) );
        setToolBar( QColor( toolBar.toString( QStringLiteral( "#757575" ) ) ) );
        setCard( QColor( card.toString( QStringLiteral( "#fafafa" ) ) ) );
        setCardHighlight(
            QColor( cardHighlight.toString( QStringLiteral( "#eeeeee" ) ) ) );
}

void Color::setApp( const QColor &app )
{
        if ( app not_eq m_app ) {
                m_app = app;
                emit appChanged();
        }
}

void Color::setAppBar( const QColor &appBar )
{
        if ( appBar not_eq m_appBar ) {
                m_appBar = appBar;
                emit appBarChanged();
        }
}

void Color::setCard( const QColor &card )
{
        if ( card not_eq m_card ) {
                m_card = card;
                emit cardChanged();
                emit isDarkChanged();
        }
}

void Color::setCardHighlight( const QColor &cardHighlight )
{
        if ( cardHighlight not_eq m_cardHighlight ) {
                m_cardHighlight = cardHighlight;
                emit cardHighlightChanged();
        }
}

void Color::setToolBar( const QColor &toolBar )
{
        if ( toolBar not_eq m_toolBar ) {
                m_toolBar = toolBar;
                emit toolBarChanged();
        }
}

void Color::setAccent( const QColor &accent )
{
        if ( accent not_eq m_accent ) {
                m_accent = accent;
                emit accentChanged();
        }
}

void Color::setRipple( const QColor &ripple )
{
        if ( ripple not_eq m_ripple ) {
                m_ripple = ripple;
                emit rippleChanged();
        }
}

void Color::setPrimary( const QColor &primary )
{
        if ( primary not_eq m_primary ) {
                m_primary = primary;
                emit primaryChanged();
        }
}

void Color::setContent( const QColor &content )
{
        if ( content not_eq m_content ) {
                m_content = content;
                m_divider = m_content;
                m_disabled = m_content;
                m_hintText = m_content;
                m_secondaryText = m_content;
                m_text = m_content;
                m_icon = m_content;

                if ( isDark( m_content ) ) {
                        m_divider.setAlphaF( 0.12 );
                        m_disabled.setAlphaF( 0.26 );
                        m_hintText.setAlphaF( 0.26 );
                        m_secondaryText.setAlphaF( 0.54 );
                        m_text.setAlphaF( 0.87 );
                        m_icon.setAlphaF( 0.54 );
                } else {
                        m_divider.setAlphaF( 0.12 );
                        m_disabled.setAlphaF( 0.30 );
                        m_hintText.setAlphaF( 0.30 );
                        m_secondaryText.setAlphaF( 0.70 );
                        m_text.setAlphaF( 1.0 );
                        m_icon.setAlphaF( 1.0 );
                }

                emit contentChanged();
        }
}

void Color::setCorporation( const QColor &corporation )
{
        if ( corporation not_eq m_corporation ) {
                m_corporation = corporation;
                emit corporationChanged();
        }
}

void Color::setRunner( const QColor &runner )
{
        if ( runner not_eq m_runner ) {
                m_runner = runner;
                emit runnerChanged();
        }
}

void Color::setHaasBioroid( const QColor &haasBioroid )
{
        if ( haasBioroid not_eq m_haasBioroid ) {
                m_haasBioroid = haasBioroid;
                emit haasBioroidChanged();
        }
}

void Color::setJinteki( const QColor &jinteki )
{
        if ( jinteki not_eq m_jinteki ) {
                m_jinteki = jinteki;
                emit jintekiChanged();
        }
}

void Color::setNbn( const QColor &nbn )
{
        if ( nbn not_eq m_nbn ) {
                m_nbn = nbn;
                emit nbnChanged();
        }
}

void Color::setWeylandConsortium( const QColor &weylandConsortium )
{
        if ( weylandConsortium not_eq m_weylandConsortium ) {
                m_weylandConsortium = weylandConsortium;
                emit weylandConsortiumChanged();
        }
}

void Color::setAnarch( const QColor &anarch )
{
        if ( anarch not_eq m_anarch ) {
                m_anarch = anarch;
                emit anarchChanged();
        }
}

void Color::setCriminal( const QColor &criminal )
{
        if ( criminal not_eq m_criminal ) {
                m_criminal = criminal;
                emit criminalChanged();
        }
}

void Color::setShaper( const QColor &shaper )
{
        if ( shaper not_eq m_shaper ) {
                m_shaper = shaper;
                emit shaperChanged();
        }
}

void Color::setNeutral( const QColor &neutral )
{
        if ( neutral not_eq m_neutral ) {
                m_neutral = neutral;
                emit neutralChanged();
        }
}

bool Color::isDark( const QColor &color ) const
{
        qreal luminance = 0.2126 * color.redF() + 0.7152 * color.greenF() +
                          0.0722 * color.blueF();
        return ( luminance < 0.5 );
}

bool Color::isLight( const QColor &color ) const
{
        return !isDark( color );
}

} // namespace ui
} // namespace lp
