// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef LP_UI_COLOR_HPP
#define LP_UI_COLOR_HPP

#include <QtCore/QObject>
#include <QtGui/QColor>

namespace lp {
namespace ui {

class Color : public QObject {
        Q_OBJECT

        Q_PROPERTY( QColor app READ app NOTIFY appChanged )
        Q_PROPERTY( QColor appBar READ appBar NOTIFY appBarChanged )
        Q_PROPERTY( QColor card READ card NOTIFY cardChanged )
        Q_PROPERTY( QColor cardHighlight READ cardHighlight NOTIFY
                        cardHighlightChanged )
        Q_PROPERTY( QColor toolBar READ toolBar NOTIFY toolBarChanged )
        Q_PROPERTY( QColor accent READ accent NOTIFY accentChanged )
        Q_PROPERTY( QColor ripple READ ripple NOTIFY rippleChanged )
        Q_PROPERTY( QColor primary READ primary NOTIFY primaryChanged )

        Q_PROPERTY( QColor divider READ divider NOTIFY contentChanged )
        Q_PROPERTY( QColor disabled READ disabled NOTIFY contentChanged )
        Q_PROPERTY( QColor hintText READ hintText NOTIFY contentChanged )
        Q_PROPERTY( QColor secondaryText READ secondaryText NOTIFY
                        contentChanged )
        Q_PROPERTY( QColor text READ text NOTIFY contentChanged )
        Q_PROPERTY( QColor icon READ icon NOTIFY contentChanged )

        Q_PROPERTY( QColor corporation READ corporation NOTIFY
                        corporationChanged )
        Q_PROPERTY( QColor runner READ runner NOTIFY runnerChanged )
        Q_PROPERTY( QColor haasBioroid READ haasBioroid NOTIFY
                        haasBioroidChanged )
        Q_PROPERTY( QColor jinteki READ jinteki NOTIFY jintekiChanged )
        Q_PROPERTY( QColor nbn READ nbn NOTIFY nbnChanged )
        Q_PROPERTY( QColor weylandConsortium READ weylandConsortium NOTIFY
                        weylandConsortiumChanged )
        Q_PROPERTY( QColor anarch READ anarch NOTIFY anarchChanged )
        Q_PROPERTY( QColor criminal READ criminal NOTIFY criminalChanged )
        Q_PROPERTY( QColor shaper READ shaper NOTIFY shaperChanged )
        Q_PROPERTY( QColor neutral READ neutral NOTIFY neutralChanged )
        Q_PROPERTY( bool isDark READ isDark NOTIFY isDarkChanged )

      public:
        explicit Color( QObject *parent = 0 );

        Q_INVOKABLE QColor faction( const QString &faction ) const;
        Q_INVOKABLE QColor dividerFrom( const QColor &background );
        Q_INVOKABLE QColor disabledFrom( const QColor &background );
        Q_INVOKABLE QColor hintTextFrom( const QColor &background );
        Q_INVOKABLE QColor secondaryTextFrom( const QColor &background );
        Q_INVOKABLE QColor textFrom( const QColor &background );
        Q_INVOKABLE QColor iconFrom( const QColor &background );

        void fromJson( const QJsonValue &jsonValue );

        QColor app() const;
        void setApp( const QColor &app );
        QColor appBar() const;
        void setAppBar( const QColor &appBar );
        QColor card() const;
        void setCard( const QColor &card );
        QColor cardHighlight() const;
        void setCardHighlight( const QColor &cardHighlight );
        QColor toolBar() const;
        void setToolBar( const QColor &toolBar );
        QColor accent() const;
        void setAccent( const QColor &accent );
        QColor ripple() const;
        void setRipple( const QColor &ripple );
        QColor primary() const;
        void setPrimary( const QColor &primary );
        QColor divider() const;
        QColor disabled() const;
        QColor hintText() const;
        QColor secondaryText() const;
        QColor text() const;
        QColor icon() const;
        void setContent( const QColor &content );
        QColor corporation() const;
        void setCorporation( const QColor &corporation );
        QColor runner() const;
        void setRunner( const QColor &runner );
        QColor haasBioroid() const;
        void setHaasBioroid( const QColor &haasBioroid );
        QColor jinteki() const;
        void setJinteki( const QColor &jinteki );
        QColor nbn() const;
        void setNbn( const QColor &nbn );
        QColor weylandConsortium() const;
        void setWeylandConsortium( const QColor &weylandConsortium );
        QColor anarch() const;
        void setAnarch( const QColor &anarch );
        QColor criminal() const;
        void setCriminal( const QColor &criminal );
        QColor shaper() const;
        void setShaper( const QColor &shaper );
        QColor neutral() const;
        void setNeutral( const QColor &neutral );
        bool isDark() const;

signals:
        void appChanged();
        void appBarChanged();
        void cardChanged();
        void cardHighlightChanged();
        void toolBarChanged();
        void accentChanged();
        void rippleChanged();
        void primaryChanged();
        void contentChanged();
        void corporationChanged();
        void runnerChanged();
        void haasBioroidChanged();
        void jintekiChanged();
        void nbnChanged();
        void weylandConsortiumChanged();
        void anarchChanged();
        void criminalChanged();
        void shaperChanged();
        void neutralChanged();
        void isDarkChanged();

      private:
        QColor m_app;
        QColor m_appBar;
        QColor m_card;
        QColor m_cardHighlight;
        QColor m_toolBar;
        QColor m_accent;
        QColor m_ripple;
        QColor m_primary;
        QColor m_content;
        QColor m_divider;
        QColor m_disabled;
        QColor m_hintText;
        QColor m_secondaryText;
        QColor m_text;
        QColor m_icon;
        QColor m_corporation;
        QColor m_runner;
        QColor m_haasBioroid;
        QColor m_jinteki;
        QColor m_nbn;
        QColor m_weylandConsortium;
        QColor m_anarch;
        QColor m_criminal;
        QColor m_shaper;
        QColor m_neutral;
        bool m_isDark;

        bool isDark( const QColor &color ) const;
        bool isLight( const QColor &color ) const;
};

inline QColor Color::app() const
{
        return m_app;
}

inline QColor Color::appBar() const
{
        return m_appBar;
}

inline QColor Color::card() const
{
        return m_card;
}

inline QColor Color::cardHighlight() const
{
        return m_cardHighlight;
}

inline QColor Color::toolBar() const
{
        return m_toolBar;
}

inline QColor Color::accent() const
{
        return m_accent;
}

inline QColor Color::ripple() const
{
        return m_ripple;
}

inline QColor Color::primary() const
{
        return m_primary;
}

inline QColor Color::divider() const
{
        return m_divider;
}

inline QColor Color::disabled() const
{
        return m_disabled;
}

inline QColor Color::hintText() const
{
        return m_hintText;
}

inline QColor Color::secondaryText() const
{
        return m_secondaryText;
}

inline QColor Color::text() const
{
        return m_text;
}

inline QColor Color::icon() const
{
        return m_icon;
}

inline QColor Color::corporation() const
{
        return m_corporation;
}

inline QColor Color::runner() const
{
        return m_runner;
}

inline QColor Color::haasBioroid() const
{
        return m_haasBioroid;
}

inline QColor Color::jinteki() const
{
        return m_jinteki;
}

inline QColor Color::nbn() const
{
        return m_nbn;
}

inline QColor Color::weylandConsortium() const
{
        return m_weylandConsortium;
}

inline QColor Color::anarch() const
{
        return m_anarch;
}

inline QColor Color::criminal() const
{
        return m_criminal;
}

inline QColor Color::shaper() const
{
        return m_shaper;
}

inline QColor Color::neutral() const
{
        return m_neutral;
}

inline bool Color::isDark() const
{
        return isDark( card() );
}

} // namespace ui
} // namespace lp

#endif // LP_UI_COLOR_HPP
