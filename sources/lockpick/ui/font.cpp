// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "lockpick/ui/font.hpp"

#include <QtGui/QFontDatabase>

namespace lp {
namespace ui {

Font::Font( QObject *parent )
    : QObject( parent )
    , m_title()
    , m_subhead()
    , m_body2()
    , m_body1()
    , m_button()
    , m_sp( 0.0 )
{
        setupFonts();
}

void Font::setSp( const qreal &sp )
{
        if ( sp not_eq m_sp ) {
                m_sp = sp;
                setFontsSize();
        }
}

void Font::setFontsSize()
{
        m_title.setPixelSize( 20 * m_sp );
        m_subhead.setPixelSize( 15 * m_sp );
        m_body2.setPixelSize( 13 * m_sp );
        m_body1.setPixelSize( 13 * m_sp );
        m_button.setPixelSize( 14 * m_sp );
}

void Font::setupFonts()
{
        QFontDatabase fontDatabase;

        fontDatabase.addApplicationFont(
            QStringLiteral( ":/fonts/Roboto-Medium.ttf" ) );
        fontDatabase.addApplicationFont(
            QStringLiteral( ":/fonts/Roboto-Regular.ttf" ) );

#ifdef Q_OS_WIN

        m_title.setFamily( QStringLiteral( "Roboto Medium" ) );
        m_subhead.setFamily( QStringLiteral( "Roboto Regular" ) );
        m_body2.setFamily( QStringLiteral( "Roboto Medium" ) );
        m_body1.setFamily( QStringLiteral( "Roboto Regular" ) );
        m_button.setFamily( QStringLiteral( "Roboto Medium" ) );

#else

        m_title.setFamily( QStringLiteral( "Roboto" ) );
        m_title.setStyleName( QStringLiteral( "Medium" ) );

        m_subhead.setFamily( QStringLiteral( "Roboto" ) );
        m_subhead.setStyleName( QStringLiteral( "Regular" ) );

        m_body2.setFamily( QStringLiteral( "Roboto" ) );
        m_body2.setStyleName( QStringLiteral( "Medium" ) );

        m_body1.setFamily( QStringLiteral( "Roboto" ) );
        m_body1.setStyleName( QStringLiteral( "Regular" ) );

        m_button.setFamily( QStringLiteral( "Roboto" ) );
        m_button.setStyleName( QStringLiteral( "Medium" ) );

#endif

        m_button.setCapitalization( QFont::AllUppercase );
}

} // namespace ui
} // namespace lp
