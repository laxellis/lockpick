// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef LP_UI_FONT_HPP
#define LP_UI_FONT_HPP

#include <QtCore/QObject>
#include <QtGui/QFont>

namespace lp {
namespace ui {

class Font : public QObject {
        Q_OBJECT

        Q_PROPERTY( QFont title READ title CONSTANT )
        Q_PROPERTY( QFont subhead READ subhead CONSTANT )
        Q_PROPERTY( QFont body2 READ body2 CONSTANT )
        Q_PROPERTY( QFont body1 READ body1 CONSTANT )
        Q_PROPERTY( QFont button READ button CONSTANT )

      public:
        explicit Font( QObject *parent = 0 );

        QFont title() const;
        QFont subhead() const;
        QFont body2() const;
        QFont body1() const;
        QFont button() const;

        void setSp( const qreal &sp );

      private:
        QFont m_title;
        QFont m_subhead;
        QFont m_body2;
        QFont m_body1;
        QFont m_button;

        qreal m_sp;

        void setFontsSize();
        void setupFonts();
};

inline QFont Font::title() const
{
        return m_title;
}

inline QFont Font::subhead() const
{
        return m_subhead;
}

inline QFont Font::body2() const
{
        return m_body2;
}

inline QFont Font::body1() const
{
        return m_body1;
}

inline QFont Font::button() const
{
        return m_button;
}

} // namespace ui
} // namespace lp

#endif // LP_UI_FONT_HPP
