// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef LP_UI_IMAGE_HPP
#define LP_UI_IMAGE_HPP

#include <QtCore/QObject>

namespace lp {
namespace ui {

class Image : public QObject {
        Q_OBJECT

        Q_PROPERTY( QString add READ add CONSTANT )
        Q_PROPERTY( QString arrowBack READ arrowBack CONSTANT )
        Q_PROPERTY( QString cancel READ cancel CONSTANT )
        Q_PROPERTY( QString checkBoxOff READ checkBoxOff CONSTANT )
        Q_PROPERTY( QString checkBoxOn READ checkBoxOn CONSTANT )
        Q_PROPERTY( QString clear READ clear CONSTANT )
        Q_PROPERTY( QString dropDown READ dropDown CONSTANT )
        Q_PROPERTY( QString expandLess READ expandLess CONSTANT )
        Q_PROPERTY( QString expandMore READ expandMore CONSTANT )
        Q_PROPERTY( QString filter READ filter CONSTANT )
        Q_PROPERTY( QString help READ help CONSTANT )
        Q_PROPERTY( QString lockpick READ lockpick CONSTANT )
        Q_PROPERTY( QString more READ more CONSTANT )
        Q_PROPERTY( QString next READ next CONSTANT )
        Q_PROPERTY( QString newDeck READ newDeck CONSTANT )
        Q_PROPERTY( QString open READ open CONSTANT )
        Q_PROPERTY( QString previous READ previous CONSTANT )
        Q_PROPERTY( QString save READ save CONSTANT )
        Q_PROPERTY( QString search READ search CONSTANT )
        Q_PROPERTY( QString settings READ settings CONSTANT )
        Q_PROPERTY( QString sort READ sort CONSTANT )
        Q_PROPERTY( QString style READ style CONSTANT )
        Q_PROPERTY( QString top READ top CONSTANT )
        Q_PROPERTY( QString runner READ runner CONSTANT )
        Q_PROPERTY( QString corporation READ corporation CONSTANT )
        Q_PROPERTY( QString anarch READ anarch CONSTANT )
        Q_PROPERTY( QString criminal READ criminal CONSTANT )
        Q_PROPERTY( QString shaper READ shaper CONSTANT )
        Q_PROPERTY( QString haasBioroid READ haasBioroid CONSTANT )
        Q_PROPERTY( QString jinteki READ jinteki CONSTANT )
        Q_PROPERTY( QString nbn READ nbn CONSTANT )
        Q_PROPERTY( QString weylandConsortium READ weylandConsortium CONSTANT )
        Q_PROPERTY( QString runnerIdentity READ runnerIdentity CONSTANT )
        Q_PROPERTY( QString program READ program CONSTANT )
        Q_PROPERTY( QString hardware READ hardware CONSTANT )
        Q_PROPERTY( QString resource READ resource CONSTANT )
        Q_PROPERTY( QString event READ runnerEvent CONSTANT )
        Q_PROPERTY( QString corporationIdentity READ corporationIdentity
                        CONSTANT )
        Q_PROPERTY( QString agenda READ agenda CONSTANT )
        Q_PROPERTY( QString ice READ ice CONSTANT )
        Q_PROPERTY( QString asset READ asset CONSTANT )
        Q_PROPERTY( QString upgrade READ upgrade CONSTANT )
        Q_PROPERTY( QString operation READ operation CONSTANT )
        Q_PROPERTY( QString coreSet READ coreSet CONSTANT )
        Q_PROPERTY( QString genesis READ genesis CONSTANT )
        Q_PROPERTY( QString creationAndControl READ creationAndControl
                        CONSTANT )
        Q_PROPERTY( QString spin READ spin CONSTANT )
        Q_PROPERTY( QString honorAndProfit READ honorAndProfit CONSTANT )
        Q_PROPERTY( QString lunar READ lunar CONSTANT )
        Q_PROPERTY( QString orderAndChaos READ orderAndChaos CONSTANT )
        Q_PROPERTY( QString credit READ credit CONSTANT )
        Q_PROPERTY( QString baseLink READ baseLink CONSTANT )
        Q_PROPERTY( QString minimumDeckSize READ minimumDeckSize CONSTANT )
        Q_PROPERTY( QString influenceLimit READ influenceLimit CONSTANT )
        Q_PROPERTY( QString rez READ rez CONSTANT )
        Q_PROPERTY( QString trash READ trash CONSTANT )
        Q_PROPERTY( QString memory READ memory CONSTANT )
        Q_PROPERTY( QString strength READ strength CONSTANT )
        Q_PROPERTY( QString advancementRequirement READ advancementRequirement
                        CONSTANT )
        Q_PROPERTY( QString agendaPoints READ agendaPoints CONSTANT )

      public:
        explicit Image( QObject *parent = 0 );

        QString add() const;
        QString arrowBack() const;
        QString cancel() const;
        QString checkBoxOff() const;
        QString checkBoxOn() const;
        QString clear() const;
        QString dropDown() const;
        QString expandLess() const;
        QString expandMore() const;
        QString filter() const;
        QString help() const;
        QString lockpick() const;
        QString more() const;
        QString next() const;
        QString newDeck() const;
        QString open() const;
        QString previous() const;
        QString save() const;
        QString search() const;
        QString settings() const;
        QString sort() const;
        QString style() const;
        QString top() const;
        QString runner() const;
        QString corporation() const;
        QString anarch() const;
        QString criminal() const;
        QString shaper() const;
        QString haasBioroid() const;
        QString jinteki() const;
        QString nbn() const;
        QString weylandConsortium() const;
        QString runnerIdentity() const;
        QString program() const;
        QString hardware() const;
        QString resource() const;
        QString runnerEvent() const;
        QString corporationIdentity() const;
        QString agenda() const;
        QString ice() const;
        QString asset() const;
        QString upgrade() const;
        QString operation() const;
        QString coreSet() const;
        QString genesis() const;
        QString creationAndControl() const;
        QString spin() const;
        QString honorAndProfit() const;
        QString lunar() const;
        QString orderAndChaos() const;
        QString credit() const;
        QString baseLink() const;
        QString minimumDeckSize() const;
        QString influenceLimit() const;
        QString rez() const;
        QString trash() const;
        QString memory() const;
        QString strength() const;
        QString advancementRequirement() const;
        QString agendaPoints() const;
};

inline QString Image::add() const
{
        return QLatin1String( "/images/ui/add.svg" );
}

inline QString Image::arrowBack() const
{
        return QLatin1String( "/images/ui/arrow_back.svg" );
}

inline QString Image::cancel() const
{
        return QLatin1String( "/images/ui/cancel.svg" );
}

inline QString Image::checkBoxOff() const
{
        return QLatin1String( "/images/ui/check_box_outline_blank.svg" );
}

inline QString Image::checkBoxOn() const
{
        return QLatin1String( "/images/ui/check_box.svg" );
}

inline QString Image::clear() const
{
        return QLatin1String( "/images/ui/clear.svg" );
}

inline QString Image::dropDown() const
{
        return QLatin1String( "/images/ui/arrow_drop_down.svg" );
}

inline QString Image::expandLess() const
{
        return QLatin1String( "/images/ui/expand_less.svg" );
}

inline QString Image::expandMore() const
{
        return QLatin1String( "/images/ui/expand_more.svg" );
}

inline QString Image::filter() const
{
        return QLatin1String( "/images/ui/filter_list.svg" );
}

inline QString Image::help() const
{
        return QLatin1String( "/images/ui/help.svg" );
}

inline QString Image::lockpick() const
{
        return QLatin1String( "/images/ui/lockpick_icon_192x192.svg" );
}

inline QString Image::more() const
{
        return QLatin1String( "/images/ui/more_vert.svg" );
}

inline QString Image::next() const
{
        return QLatin1String( "/images/ui/chevron_right.svg" );
}

inline QString Image::newDeck() const
{
        return QLatin1String( "/images/ui/add.svg" );
}

inline QString Image::open() const
{
        return QLatin1String( "/images/ui/file_download.svg" );
}

inline QString Image::previous() const
{
        return QLatin1String( "/images/ui/chevron_left.svg" );
}

inline QString Image::save() const
{
        return QLatin1String( "/images/ui/file_upload.svg" );
}

inline QString Image::search() const
{
        return QLatin1String( "/images/ui/search.svg" );
}

inline QString Image::settings() const
{
        return QLatin1String( "/images/ui/settings.svg" );
}

inline QString Image::sort() const
{
        return QLatin1String( "/images/ui/sort.svg" );
}

inline QString Image::style() const
{
        return QLatin1String( "/images/ui/style.svg" );
}

inline QString Image::top() const
{
        return QLatin1String( "/images/ui/expand_less.svg" );
}

inline QString Image::runner() const
{
        return QLatin1String( "/images/ui/keyboard.svg" );
}

inline QString Image::corporation() const
{
        return QLatin1String( "/images/ui/business.svg" );
}

inline QString Image::anarch() const
{
        return QLatin1String( "/images/ui/anarch.svg" );
}

inline QString Image::criminal() const
{
        return QLatin1String( "/images/ui/criminal.svg" );
}

inline QString Image::shaper() const
{
        return QLatin1String( "/images/ui/shaper.svg" );
}

inline QString Image::haasBioroid() const
{
        return QLatin1String( "/images/ui/haas_bioroid.svg" );
}

inline QString Image::jinteki() const
{
        return QLatin1String( "/images/ui/jinteki.svg" );
}

inline QString Image::nbn() const
{
        return QLatin1String( "/images/ui/nbn.svg" );
}

inline QString Image::weylandConsortium() const
{
        return QLatin1String( "/images/ui/weyland_consortium.svg" );
}

inline QString Image::runnerIdentity() const
{
        return QLatin1String( "/images/ui/perm_identity.svg" );
}

inline QString Image::program() const
{
        return QLatin1String( "/images/ui/sim_card.svg" );
}

inline QString Image::hardware() const
{
        return QLatin1String( "/images/ui/memory.svg" );
}

inline QString Image::resource() const
{
        return QLatin1String( "/images/ui/group.svg" );
}

inline QString Image::runnerEvent() const
{
        return QLatin1String( "/images/ui/new_releases.svg" );
}

inline QString Image::corporationIdentity() const
{
        return QLatin1String( "/images/ui/perm_identity.svg" );
}

inline QString Image::agenda() const
{
        return QLatin1String( "/images/ui/poll.svg" );
}

inline QString Image::ice() const
{
        return QLatin1String( "/images/ui/security.svg" );
}

inline QString Image::asset() const
{
        return QLatin1String( "/images/ui/group.svg" );
}

inline QString Image::upgrade() const
{
        return QLatin1String( "/images/ui/file_upload.svg" );
}

inline QString Image::operation() const
{
        return QLatin1String( "/images/ui/new_releases.svg" );
}

inline QString Image::coreSet() const
{
        return QLatin1String( "/images/ui/core_set.svg" );
}

inline QString Image::genesis() const
{
        return QLatin1String( "/images/ui/genesis.svg" );
}

inline QString Image::creationAndControl() const
{
        return QLatin1String( "/images/ui/creation_and_control.svg" );
}

inline QString Image::spin() const
{
        return QLatin1String( "/images/ui/spin.svg" );
}

inline QString Image::honorAndProfit() const
{
        return QLatin1String( "/images/ui/honor_and_profit.svg" );
}

inline QString Image::lunar() const
{
        return QLatin1String( "/images/ui/lunar.svg" );
}

inline QString Image::orderAndChaos() const
{
        return QLatin1String( "/images/ui/anarch.svg" );
}

inline QString Image::credit() const
{
        return QLatin1String( "/images/ui/credit.svg" );
}

inline QString Image::baseLink() const
{
        return QLatin1String( "/images/ui/base_link.svg" );
}

inline QString Image::minimumDeckSize() const
{
        return QLatin1String( "/images/ui/layers.svg" );
}

inline QString Image::influenceLimit() const
{
        return QLatin1String( "/images/ui/group.svg" );
}

inline QString Image::rez() const
{
        return QLatin1String( "/images/ui/blur_on.svg" );
}

inline QString Image::trash() const
{
        return QLatin1String( "/images/ui/delete.svg" );
}

inline QString Image::memory() const
{
        return QLatin1String( "/images/ui/memory.svg" );
}

inline QString Image::strength() const
{
        return QLatin1String( "/images/ui/sort.svg" );
}

inline QString Image::advancementRequirement() const
{
        return QLatin1String( "/images/ui/advancement_requirement.svg" );
}

inline QString Image::agendaPoints() const
{
        return QLatin1String( "/images/ui/poll.svg" );
}

} // namespace ui
} // namespace lp

#endif // LP_UI_IMAGE_HPP
