// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#include "lockpick/ui/ui.hpp"

#include <QtCore/QFile>
#include <QtCore/QJsonDocument>
#include <QtCore/QJsonObject>
#include <QtCore/QUrl>
#include <QtGui/QGuiApplication>
#include <QtGui/QScreen>

#include "lockpick/ui/color.hpp"
#include "lockpick/ui/font.hpp"
#include "lockpick/ui/image.hpp"

namespace lp {
namespace ui {

Ui::Ui( QObject *parent )
    : QObject( parent )
    , m_color( new Color( this ) )
    , m_font( new Font( this ) )
    , m_image( new Image( this ) )
    , m_dp( 1.0 )
{
        computeDp();
        font()->setSp( m_dp );
}

void Ui::loadTheme( const QUrl &url )
{
        QFile file( url.path().prepend( ':' ) );

        file.open( QIODevice::ReadOnly );

        QByteArray data( file.readAll() );

        QJsonDocument jsonDocument( QJsonDocument::fromJson( data ) );
        QJsonObject jsonObject( jsonDocument.object() );

        color()->fromJson( jsonObject.value( QStringLiteral( "colors" ) ) );

        emit styleChanged( color()->primary().name(), color()->isDark() );
}

void Ui::computeDp()
{
        // 48 dp = 9 mm
        //  9 mm = 0.354331 in

        qreal dpi( QGuiApplication::primaryScreen()->physicalDotsPerInch() );
        m_dp = ( dpi * 0.354331 ) / 48;
        if ( dp() < 1.0 ) {
                m_dp = 1.0;
        }
}

} // namespace ui
} // namespace lp
