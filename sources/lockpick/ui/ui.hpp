// Copyright © 2015 Alexis Lecharpentier

// Permission is hereby granted, free of charge, to any person obtaining a copy
// of this software and associated documentation files (the "Software"), to deal
// in the Software without restriction, including without limitation the rights
// to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
// copies of the Software, and to permit persons to whom the Software is
// furnished to do so, subject to the following conditions:

// The above copyright notice and this permission notice shall be included in
// all copies or substantial portions of the Software.

// THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
// IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
// FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
// AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
// LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
// OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
// SOFTWARE.

#ifndef LP_UI_UI_HPP
#define LP_UI_UI_HPP

#include <QtCore/QObject>

namespace lp {
namespace ui {

class Color;
class Font;
class Image;

class Ui : public QObject {
        Q_OBJECT

        Q_PROPERTY( lp::ui::Color *color READ color CONSTANT )
        Q_PROPERTY( lp::ui::Font *font READ font CONSTANT )
        Q_PROPERTY( lp::ui::Image *image READ image CONSTANT )
        Q_PROPERTY( qreal dp READ dp CONSTANT )

      public:
        explicit Ui( QObject *parent = 0 );

        Q_INVOKABLE void loadTheme( const QUrl &url );

        Color *color() const;
        Font *font() const;
        Image *image() const;
        qreal dp() const;

signals:
        void styleChanged( const QString &primaryColor, const bool &isDark );

      private:
        Color *m_color;
        Font *m_font;
        Image *m_image;
        qreal m_dp;

        void computeDp();
};

inline Color *Ui::color() const
{
        return m_color;
}

inline Font *Ui::font() const
{
        return m_font;
}

inline Image *Ui::image() const
{
        return m_image;
}

inline qreal Ui::dp() const
{
        return m_dp;
}

} // namespace ui
} // namespace lp

#endif // LP_UI_UI_HPP
